﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPSTaskScheduler.Models
{
    public class JobDescription
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Action { get; set; }
        public string Category { get; set; }
        public Boolean AllowSkip { get; set; }
        public string FactorName { get; set; }
        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public DateTime Modified { get; set; }
        public string ModifiedBy { get; set; }

        #region SELECTS
        public static JobDescription selJobDescriptionByID(int id)
        {
            JobDescription definition = new JobDescription();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selJobDescriptionById", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("ID", id);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        try
                        {
                            definition.Created = DateTime.Parse(sdr["Created"].ToString());
                        }
                        catch { }

                        definition.ID = id;
                        definition.Title = sdr["Title"].ToString();
                        definition.CreatedBy = sdr["CreatedBy"].ToString();
                        definition.Description = sdr["Description"].ToString();
                        definition.Action = sdr["Action"].ToString();                        
                        definition.Category = sdr["Category"].ToString();
                        definition.FactorName = sdr["FactorName"].ToString();
                        try
                        {
                            definition.AllowSkip = Boolean.Parse(sdr["AllowSkip"].ToString());
                        } catch { definition.AllowSkip = false; }
                        definition.ModifiedBy = sdr["ModifiedBy"].ToString();
                        try
                        {
                            definition.Modified = DateTime.Parse(sdr["Modified"].ToString());
                        }
                        catch { }

                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return definition;
        }

        public static List<JobDescription> selJobDescriptions()
        {
            List<JobDescription> definitions = new List<JobDescription>();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selJobDescriptions", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        JobDescription definition = new JobDescription();
                        try
                        {
                            definition.Created = DateTime.Parse(sdr["Created"].ToString());
                        }
                        catch { }

                        definition.ID = int.Parse(sdr["ID"].ToString());
                        definition.Title = sdr["Title"].ToString();
                        definition.CreatedBy = sdr["CreatedBy"].ToString();
                        definition.Description = sdr["Description"].ToString();
                        definition.Action = sdr["Action"].ToString();
                        definition.Category = sdr["Category"].ToString();
                        definition.FactorName = sdr["FactorName"].ToString();
                        definition.AllowSkip = Boolean.Parse(sdr["AllowSkip"].ToString());
                        definition.ModifiedBy = sdr["ModifiedBy"].ToString();
                        try
                        {
                            definition.Modified = DateTime.Parse(sdr["Modified"].ToString());
                        }
                        catch { }


                        definitions.Add(definition);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return definitions;
        }

        public static List<string> selFactorNames(int id)
        {
            List<string> factors = new List<string>();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selFactorName", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        factors.Add(sdr["FactorName"].ToString());
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return factors;
        }

        #endregion

        #region INSERTS & UPDATES
        public static string insJobDescription(JobDescription JobDescription)
        {
            string id = "";

            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.insJobDescription";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("Title", JobDescription.Title);
                    sqlcmd.Parameters.AddWithValue("Description", JobDescription.Description);
                    sqlcmd.Parameters.AddWithValue("Action", JobDescription.Action);
                    sqlcmd.Parameters.AddWithValue("AllowSkip", JobDescription.AllowSkip);
                    sqlcmd.Parameters.AddWithValue("Category", JobDescription.Category);
                    sqlcmd.Parameters.AddWithValue("FactorName", JobDescription.FactorName);
                    sqlcmd.Parameters.AddWithValue("Created", DateTime.Now);
                    sqlcmd.Parameters.AddWithValue("CreatedBy", JobDescription.CreatedBy);
                    sqlcmd.Parameters.AddWithValue("Modified", DateTime.Now);
                    sqlcmd.Parameters.AddWithValue("ModifiedBy", JobDescription.ModifiedBy);

                    id = sqlcmd.ExecuteScalar().ToString();

                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {

                }
            }//end sql

            return id;
        }

        public static void updJobDescription(JobDescription JobDescription)
        {
            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.updJobDescription";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("ID", JobDescription.ID);
                    sqlcmd.Parameters.AddWithValue("Title", JobDescription.Title);
                    sqlcmd.Parameters.AddWithValue("Description", JobDescription.Description);
                    sqlcmd.Parameters.AddWithValue("Action", JobDescription.Action);
                    sqlcmd.Parameters.AddWithValue("AllowSkip", JobDescription.AllowSkip);
                    sqlcmd.Parameters.AddWithValue("Category", JobDescription.Category);
                    sqlcmd.Parameters.AddWithValue("FactorName", JobDescription.FactorName);
                    sqlcmd.Parameters.AddWithValue("Modified", DateTime.Now);
                    sqlcmd.Parameters.AddWithValue("ModifiedBy", JobDescription.ModifiedBy);

                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {

                }
            }//end sql
        }
        #endregion

        #region DELETES
        public static void delJobDescription(JobDescription JobDescription)
        {
            int i = 0;
            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.delJobDescription";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("ID", JobDescription.ID);
                    i = sqlcmd.ExecuteNonQuery();
                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {

                }
            }//end sql
        }
        #endregion
    }
}
