﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace OPSTaskScheduler.Models
{
    public class JobTasks
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public int JobDescriptionID { get; set; }
        public List<String> DaysOfWeek { get; set; }
        public string Frequency { get; set; }
        public int Intervals { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<Subtask> Subtasks { get; set; }
        public JobDescription JobDescription { get; set; }
        public int ReplaceTaskID { get; set; }
        public DateTime Modified { get; set; }
        public string ModifiedBy { get; set; }
        public Boolean HasSubtasks { get; set; }
        public string Month { get; set; }
        public int Day { get; set; }
        public string Placement { get; set; }

        #region SELECTS
        public static JobTasks selJobTask(int id)
        {
            JobTasks task = new JobTasks();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selJobTaskByID", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("ID", id);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {

                        task.ID = id;

                        try
                        {
                            task.Created = DateTime.Parse(sdr["Created"].ToString());
                        }
                        catch { }

                        try
                        {
                            task.Modified = DateTime.Parse(sdr["Modified"].ToString());
                        }
                        catch { }

                        task.Title = sdr["Title"].ToString();
                        task.CreatedBy = sdr["CreatedBy"].ToString();
                        task.ModifiedBy = sdr["ModifiedBy"].ToString();
                        task.Description = sdr["Description"].ToString();
                        task.Frequency = sdr["Frequency"].ToString();
                        task.Month = sdr["Month"].ToString();
                        task.Placement = sdr["Placement"].ToString();
                        try
                        {
                            task.HasSubtasks = Boolean.Parse(sdr["HasSubtasks"].ToString());
                        }
                        catch { task.HasSubtasks = false; }                        
                        try
                        {
                            task.Day = int.Parse(sdr["Day"].ToString());
                        }
                        catch { task.Day = 0; }
                        try
                        {
                            task.JobDescriptionID = int.Parse(sdr["JobDescriptionID"].ToString());
                        }
                        catch { task.JobDescriptionID = 0; }

                        try
                        {
                            task.Intervals = int.Parse(sdr["Intervals"].ToString());
                        }
                        catch { task.Intervals = 1; }
                        try
                        {
                            task.ReplaceTaskID = int.Parse(sdr["ReplaceTaskID"].ToString());
                        }
                        catch { task.ReplaceTaskID = 0; }
                        try
                        {
                            task.StartDate = DateTime.Parse(sdr["StartDate"].ToString());
                        }
                        catch { }
                        try
                        {
                            task.EndDate = DateTime.Parse(sdr["EndDate"].ToString());
                        }
                        catch { }

                        task.DaysOfWeek = selDaysOfWeek(id);                        
                        task.JobDescription = JobDescription.selJobDescriptionByID(task.JobDescriptionID);

                        if(task.HasSubtasks)
                            task.Subtasks = Subtask.selSubtask_Lookup(id);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return task;
        }

        public static List<JobTasks> selJobTasks()
        {
            List<JobTasks> tasks = new List<JobTasks>();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selJobTasks", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    //sqlcmd.Parameters.AddWithValue("ID", id);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        JobTasks task = new JobTasks();
                       

                        task.Title = sdr["Title"].ToString();
                        task.CreatedBy = sdr["CreatedBy"].ToString();
                        task.ModifiedBy = sdr["ModifiedBy"].ToString();
                        task.Description = sdr["Description"].ToString();
                        task.Frequency = sdr["Frequency"].ToString();
                        task.ID = int.Parse(sdr["ID"].ToString());                        
                        task.Month = sdr["Month"].ToString();
                        task.Placement = sdr["Placement"].ToString();
                        try
                        {
                            task.Created = DateTime.Parse(sdr["Created"].ToString());
                        }
                        catch { }
                        try
                        {
                            task.Modified = DateTime.Parse(sdr["Modified"].ToString());
                        }
                        catch { }
                        try
                        {
                            task.HasSubtasks = Boolean.Parse(sdr["HasSubtasks"].ToString());
                        } catch { task.HasSubtasks = false; }
                        try
                        {
                            task.Day = int.Parse(sdr["Day"].ToString());
                        }
                        catch { task.Day = 0; }
                        try
                        {
                            task.JobDescriptionID = int.Parse(sdr["JobDescriptionID"].ToString());
                        }catch { task.JobDescriptionID = 0; }

                        try
                        {
                            task.Intervals = int.Parse(sdr["Intervals"].ToString());
                        }catch { task.Intervals = 1; }
                        try
                        {
                            task.ReplaceTaskID = int.Parse(sdr["ReplaceTaskID"].ToString());
                        }catch { task.ReplaceTaskID = 0; }
                        try
                        {
                            task.StartDate = DateTime.Parse(sdr["StartDate"].ToString());
                        } catch { }
                        try
                        {
                            task.EndDate = DateTime.Parse(sdr["EndDate"].ToString());
                        }
                        catch { }

                        task.DaysOfWeek = selDaysOfWeek(task.ID);
                        task.JobDescription = JobDescription.selJobDescriptionByID(task.JobDescriptionID);

                        if (task.HasSubtasks)
                            task.Subtasks = Subtask.selSubtask_Lookup(task.ID);
                                               

                        tasks.Add(task);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return tasks;
        }

        public static List<string> selDaysOfWeek(int id)
        {
            List<string> days = new List<string>();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selDaysOfWeek_Lookup_ByID", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("TaskID", id);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        days.Add(sdr["DayOfWeek"].ToString());                        
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return days;
        }        

        #endregion

        #region INSERTS & UPDATES
        public static string insJobTask(JobTasks JobTask)
        {
            string id = "";

            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.insJobTask";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("Title", JobTask.Title);
                    sqlcmd.Parameters.AddWithValue("Description", JobTask.Title);
                    sqlcmd.Parameters.AddWithValue("JobDescriptionID", JobTask.JobDescriptionID);
                    sqlcmd.Parameters.AddWithValue("Frequency", JobTask.Frequency);
                    sqlcmd.Parameters.AddWithValue("Intervals", JobTask.Intervals);
                    sqlcmd.Parameters.AddWithValue("CreatedBy", JobTask.CreatedBy);
                    sqlcmd.Parameters.AddWithValue("StartDate", JobTask.StartDate);
                    sqlcmd.Parameters.AddWithValue("EndDate", JobTask.EndDate);
                    sqlcmd.Parameters.AddWithValue("ReplaceTaskID", JobTask.ReplaceTaskID);
                    sqlcmd.Parameters.AddWithValue("Created", DateTime.Now);
                    sqlcmd.Parameters.AddWithValue("Modified", DateTime.Now);
                    sqlcmd.Parameters.AddWithValue("ModifiedBy", JobTask.ModifiedBy);
                    sqlcmd.Parameters.AddWithValue("HasSubtasks", JobTask.HasSubtasks);

                    id = sqlcmd.ExecuteScalar().ToString();

                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {

                }
            }//end sql

            return id;
        }

        public static void updJobTask(JobTasks JobTask)
        {

            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.updJobTask";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("ID", JobTask.ID);
                    sqlcmd.Parameters.AddWithValue("Description", JobTask.Title);
                    sqlcmd.Parameters.AddWithValue("JobDescriptionID", JobTask.JobDescriptionID);
                    sqlcmd.Parameters.AddWithValue("Frequency", JobTask.Frequency);
                    sqlcmd.Parameters.AddWithValue("StartDate", JobTask.StartDate);
                    sqlcmd.Parameters.AddWithValue("EndDate", JobTask.EndDate);
                    sqlcmd.Parameters.AddWithValue("Intervals", JobTask.Intervals);
                    sqlcmd.Parameters.AddWithValue("ReplaceTaskID", JobTask.ReplaceTaskID);
                    sqlcmd.Parameters.AddWithValue("Modified", DateTime.Now);
                    sqlcmd.Parameters.AddWithValue("ModifiedBy", JobTask.ModifiedBy);
                    sqlcmd.Parameters.AddWithValue("HasSubtasks", JobTask.HasSubtasks);

                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {

                }
            }//end sql
        }

        public static string insDayOfWeek_Lookup(int JobTaskID, string DayOfWeek)
        {
            string id = "";

            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.insDayOfWeek_Lookup";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("Title", JobTaskID);
                    sqlcmd.Parameters.AddWithValue("DayOfWeek", DayOfWeek);

                    id = sqlcmd.ExecuteScalar().ToString();

                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {

                }
            }//end sql

            return id;
        }

        #endregion

        #region DELETES
        public static void delJobTask(JobTasks JobTask)
        {
            int i = 0;
            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.delJobTask";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("id", JobTask.ID);
                    i = sqlcmd.ExecuteNonQuery();
                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {

                }
            }//end sql
        }

        public static void delDayOfWeek_Lookup(JobTasks JobTask)
        {
            int i = 0;
            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.delDayofWeek_Lookup";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("TaskID", JobTask.ID);
                    sqlcmd.ExecuteNonQuery();
                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {

                }
            }//end sql
        }
        #endregion
    }
}
