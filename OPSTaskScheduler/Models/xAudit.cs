﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPSTaskScheduler.Models
{
    public class xAudit
    {
        public int ID { get; set; }
        public string Type { get; set; }
        public int TypeID { get; set; }
        public string Action { get; set; }
        public DateTime Modified { get; set; }
        public string ModifiedBy { get; set; }
        public string Comments { get; set; }

        #region SELECTS
        public static List<xAudit> selAudit()
        {
            List<xAudit> audits = new List<xAudit>();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selAudits", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    //sqlcmd.Parameters.AddWithValue("ID", id);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        xAudit audit = new xAudit();
                        try
                        {
                            audit.Modified = DateTime.Parse(sdr["Modified"].ToString());
                        }
                        catch { }

                        audit.Type = sdr["Type"].ToString();
                        audit.TypeID = int.Parse(sdr["TypeID"].ToString());
                        audit.ID = int.Parse(sdr["ID"].ToString());
                        audit.ModifiedBy = sdr["ModifiedBy"].ToString();
                        audit.Comments = sdr["Comments"].ToString();

                        audits.Add(audit);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return audits;
        }
        public static xAudit selAuditByType(string Type)
        {
            xAudit audit = new xAudit();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selAuditsByType", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("Type", Type);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                       
                        try
                        {
                            audit.Modified = DateTime.Parse(sdr["Modified"].ToString());
                        }
                        catch { }

                        audit.Type = sdr["Type"].ToString();
                        audit.TypeID = int.Parse(sdr["TypeID"].ToString());
                        audit.ID = int.Parse(sdr["ID"].ToString());
                        audit.ModifiedBy = sdr["ModifiedBy"].ToString();
                        audit.Comments = sdr["Comments"].ToString();
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return audit;
        }
        public static xAudit selAuditByType_TypeID(string Type, int TypeID)
        {
            xAudit audit = new xAudit();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selAuditsByType_TypeID", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("Type", Type);
                    sqlcmd.Parameters.AddWithValue("TypeID", TypeID);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        try
                        {
                            audit.Modified = DateTime.Parse(sdr["Modified"].ToString());
                        }
                        catch { }

                        audit.Type = sdr["Type"].ToString();
                        audit.TypeID = int.Parse(sdr["TypeID"].ToString());
                        audit.ID = int.Parse(sdr["ID"].ToString());
                        audit.ModifiedBy = sdr["ModifiedBy"].ToString();
                        audit.Comments = sdr["Comments"].ToString();
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return audit;
        }
        #endregion

        #region INSERTS
        public static string insAudit(xAudit Audit)
        {
            string id = "";

            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.insAudit";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("Type", Audit.Type);
                    sqlcmd.Parameters.AddWithValue("TypeID", Audit.TypeID);
                    sqlcmd.Parameters.AddWithValue("Action", Audit.Action);
                    sqlcmd.Parameters.AddWithValue("ModifiedBy", Audit.ModifiedBy);
                    sqlcmd.Parameters.AddWithValue("Modified", DateTime.Now);
                    sqlcmd.Parameters.AddWithValue("Comments", Audit.Comments);

                    id = sqlcmd.ExecuteScalar().ToString();

                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {

                }
            }//end sql

            return id;
        }
        #endregion
    }
}
