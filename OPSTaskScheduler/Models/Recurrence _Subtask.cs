﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPSTaskScheduler.Models
{
    class Recurrence_Subtask
    {
        public int ID { get; set; }
        public int SubtaskID { get; set; }
        public int ParentID { get; set; }
        public DateTime DueDate { get; set; }
        public string FactorNameData { get; set; }
        public DateTime Started { get; set; }
        public string StartedBy { get; set; }
        public DateTime Finished { get; set; }
        public string FinishedBy { get; set; }
        public Boolean Completed { get; set; }
        public string Comments { get; set; }
        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public Subtask Subtask { get; set; }
        public Boolean IsActive { get; set; }
        public DateTime Modified { get; set; }
        public string ModifiedBy { get; set; }

        #region SELECTS
        public static Recurrence_Subtask selRecurrence_SubtaskByID(int id)
        {
            Recurrence_Subtask recurrence = new Recurrence_Subtask();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selSubtask_Recurrence_SubtaskByID", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("ID", id);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        recurrence.ID = id;
                        recurrence.ParentID = int.Parse(sdr["TaskID"].ToString());
                        recurrence.SubtaskID = int.Parse(sdr["SubtaskID"].ToString());
                        recurrence.DueDate = DateTime.Parse(sdr["DueDate"].ToString());
                        recurrence.FactorNameData = sdr["FactorNameData"].ToString();
                        recurrence.Started = DateTime.Parse(sdr["Started"].ToString());
                        recurrence.StartedBy = sdr["StartedBy"].ToString();
                        recurrence.Finished = DateTime.Parse(sdr["Finished"].ToString());
                        recurrence.FinishedBy = sdr["FinsihedBy"].ToString();
                        recurrence.Completed = Boolean.Parse(sdr["Completed"].ToString());
                        recurrence.Comments = sdr["Comments"].ToString();
                        recurrence.IsActive = Boolean.Parse(sdr["IsActive"].ToString());
                        recurrence.Created = DateTime.Parse(sdr["Created"].ToString());
                        recurrence.CreatedBy = sdr["CreatedBy"].ToString();
                        recurrence.ModifiedBy = sdr["ModifiedBy"].ToString();
                        try
                        {
                            recurrence.Modified = DateTime.Parse(sdr["Modified"].ToString());
                        }
                        catch { }

                        recurrence.Subtask = Subtask.selSubtaskByID(recurrence.SubtaskID);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return recurrence;
        }

        public static Recurrence_Subtask selRecurrence_Subtask_ByParentID(int ParentID)
        {
            Recurrence_Subtask recurrence = new Recurrence_Subtask();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selSubtaskask_Recurrence_Subtask_ByParentID", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("ID", ParentID);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        recurrence.ParentID = ParentID;
                        recurrence.ID = int.Parse(sdr["ID"].ToString());
                        recurrence.SubtaskID = int.Parse(sdr["SubtaskID"].ToString());
                        recurrence.DueDate = DateTime.Parse(sdr["DueDate"].ToString());
                        recurrence.FactorNameData = sdr["FactorNameData"].ToString();
                        recurrence.Started = DateTime.Parse(sdr["Started"].ToString());
                        recurrence.StartedBy = sdr["StartedBy"].ToString();
                        recurrence.Finished = DateTime.Parse(sdr["Finished"].ToString());
                        recurrence.FinishedBy = sdr["FinsihedBy"].ToString();
                        recurrence.Completed = Boolean.Parse(sdr["Completed"].ToString());
                        recurrence.Comments = sdr["Comments"].ToString();
                        recurrence.IsActive = Boolean.Parse(sdr["IsActive"].ToString());
                        recurrence.Created = DateTime.Parse(sdr["Created"].ToString());
                        recurrence.CreatedBy = sdr["CreatedBy"].ToString();
                        recurrence.ModifiedBy = sdr["ModifiedBy"].ToString();
                        try
                        {
                            recurrence.Modified = DateTime.Parse(sdr["Modified"].ToString());
                        }
                        catch { }

                        recurrence.Subtask = Subtask.selSubtaskByID(recurrence.SubtaskID);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return recurrence;
        }

        public static List<Recurrence_Subtask> selRecurrence_Subtasks()
        {
            List<Recurrence_Subtask> recurrences = new List<Recurrence_Subtask>();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selSubtask_Recurrence_Subtask", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        Recurrence_Subtask recurrence = new Recurrence_Subtask();

                        recurrence.ID = int.Parse(sdr["ID"].ToString());
                        recurrence.SubtaskID = int.Parse(sdr["SubtaskID"].ToString());
                        recurrence.DueDate = DateTime.Parse(sdr["DueDate"].ToString());
                        recurrence.FactorNameData = sdr["FactorNameData"].ToString();
                        recurrence.Started = DateTime.Parse(sdr["Started"].ToString());
                        recurrence.StartedBy = sdr["StartedBy"].ToString();
                        recurrence.Finished = DateTime.Parse(sdr["Finished"].ToString());
                        recurrence.FinishedBy = sdr["FinsihedBy"].ToString();
                        recurrence.Completed = Boolean.Parse(sdr["Completed"].ToString());
                        recurrence.Comments = sdr["Comments"].ToString();
                        recurrence.IsActive = Boolean.Parse(sdr["IsActive"].ToString());
                        recurrence.Created = DateTime.Parse(sdr["Created"].ToString());
                        recurrence.CreatedBy = sdr["CreatedBy"].ToString();
                        recurrence.ModifiedBy = sdr["ModifiedBy"].ToString();
                        try
                        {
                            recurrence.Modified = DateTime.Parse(sdr["Modified"].ToString());
                        }
                        catch { }

                        recurrence.Subtask = Subtask.selSubtaskByID(recurrence.SubtaskID);

                        recurrences.Add(recurrence);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return recurrences;
        }
        #endregion

        #region INSERTS & UPDATES
        public static string insRecurrence_Subtask(Recurrence_Subtask Recurrence)
        {
            string id = "";

            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.insSubtask_Recurrence";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("ParentID", Recurrence.ParentID);
                    sqlcmd.Parameters.AddWithValue("SubtaskID", Recurrence.SubtaskID);
                    sqlcmd.Parameters.AddWithValue("DueDate", Recurrence.DueDate);
                    sqlcmd.Parameters.AddWithValue("FactorNameData", Recurrence.FactorNameData);
                    //sqlcmd.Parameters.AddWithValue("Started", Recurrence.Started);
                    //sqlcmd.Parameters.AddWithValue("StartedBy", Recurrence.StartedBy);
                    //sqlcmd.Parameters.AddWithValue("Finished", Recurrence.Finished);
                    //sqlcmd.Parameters.AddWithValue("FinishedBy", Recurrence.FinishedBy);
                    sqlcmd.Parameters.AddWithValue("Completed", Recurrence.Completed);
                    sqlcmd.Parameters.AddWithValue("Comments", Recurrence.Comments);
                    sqlcmd.Parameters.AddWithValue("IsActive", Recurrence.IsActive);
                    sqlcmd.Parameters.AddWithValue("Created", DateTime.Now);
                    sqlcmd.Parameters.AddWithValue("CreatedBy", Recurrence.CreatedBy);
                    sqlcmd.Parameters.AddWithValue("Modified", DateTime.Now);
                    sqlcmd.Parameters.AddWithValue("ModifiedBy", Recurrence.ModifiedBy);

                    id = sqlcmd.ExecuteScalar().ToString();

                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {

                }
            }//end sql

            return id;
        }

        public static void updRecurrence_Subtask(Recurrence_Subtask Recurrence)
        {
            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.updSubtask_Recurrence";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("ID", Recurrence.ID);
                    sqlcmd.Parameters.AddWithValue("TaskID", Recurrence.SubtaskID);
                    sqlcmd.Parameters.AddWithValue("DueDate", Recurrence.DueDate);
                    sqlcmd.Parameters.AddWithValue("FactorNameData", Recurrence.FactorNameData);
                    sqlcmd.Parameters.AddWithValue("Started", Recurrence.Started);
                    sqlcmd.Parameters.AddWithValue("StartedBy", Recurrence.StartedBy);
                    sqlcmd.Parameters.AddWithValue("Finished", Recurrence.Finished);
                    sqlcmd.Parameters.AddWithValue("FinishedBy", Recurrence.FinishedBy);
                    sqlcmd.Parameters.AddWithValue("Completed", Recurrence.Completed);
                    sqlcmd.Parameters.AddWithValue("Comments", Recurrence.Comments);
                    sqlcmd.Parameters.AddWithValue("IsActive", Recurrence.IsActive);
                    sqlcmd.Parameters.AddWithValue("Modified", DateTime.Now);
                    sqlcmd.Parameters.AddWithValue("ModifiedBy", Recurrence.ModifiedBy);

                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {

                }
            }//end sql
        }
        #endregion

        #region DELETES
        public static void delRecurrence_Subtask(int ID)
        {
            int i = 0;
            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.delSubtask_Recurrence";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("ID", ID);
                    i = sqlcmd.ExecuteNonQuery();
                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {

                }
            }//end sql
        }
        public static void delSubtask_Recurrence_ByTaskID(int ID)
        {
            int i = 0;
            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.delSubtask_Recurrence_ByTaskID";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("ID", ID);
                    i = sqlcmd.ExecuteNonQuery();
                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {

                }
            }//end sql
        }
        #endregion
    }
}
