﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPSTaskScheduler.Models
{
    public class Subtask
    {
        public int ID { get;set; }
        public string Title { get; set; }
        public int JobDescriptionID { get; set; }
        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public int Order { get; set; }
        public JobDescription JobDescription { get; set; }
        public DateTime Modified { get; set; }
        public string ModifiedBy { get; set; }

        #region SELECTS
        public static Subtask selSubtaskByID(int id)
        {
            Subtask task = new Subtask();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selSubtaskByID", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("ID", id);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        task.Title = sdr["Title"].ToString();
                        task.CreatedBy = sdr["CreatedBy"].ToString();
                        task.ModifiedBy = sdr["ModifiedBy"].ToString();
                        try
                        {
                            task.Modified = DateTime.Parse(sdr["Modified"].ToString());
                        } catch { }
                        try
                        {
                            task.Created = DateTime.Parse(sdr["Created"].ToString());
                        }
                        catch { }
                        try
                        {
                            task.JobDescriptionID = int.Parse(sdr["JobDescriptionID"].ToString());
                        } catch { }

                        task.JobDescription = JobDescription.selJobDescriptionByID(task.JobDescriptionID);

                        task.ID = id;
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return task;
        }

        public static List<Subtask> selSubtasks()
        {
            List<Subtask> tasks = new List<Subtask>();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selSubtasks", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    //sqlcmd.Parameters.AddWithValue("ID", id);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        Subtask task = new Subtask();
                        try
                        {
                            task.Created = DateTime.Parse(sdr["Created"].ToString());
                        }
                        catch { }

                        task.Title = sdr["Title"].ToString();
                        task.CreatedBy = sdr["CreatedBy"].ToString();
                        task.ID = int.Parse(sdr["ID"].ToString());
                        task.ModifiedBy = sdr["ModifiedBy"].ToString();
                        try
                        {
                            task.JobDescriptionID = int.Parse(sdr["JobDescriptionID"].ToString());
                        } catch { task.JobDescriptionID = 0; }
                        try
                        {
                            task.Created = DateTime.Parse(sdr["Created"].ToString());
                        } catch { }                        
                        try
                        {
                            task.Modified = DateTime.Parse(sdr["Modified"].ToString());
                        }
                        catch { }
                        task.JobDescription = JobDescription.selJobDescriptionByID(task.JobDescriptionID);

                        tasks.Add(task);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return tasks;
        }

        public static List<Subtask> selSubtask_Lookup(int JobTaskID)
        {
            List<Subtask> subtasks = new List<Subtask>();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selSubtasks_Lookup", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("JobTaskID", JobTaskID);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        Subtask sub = Subtask.selSubtaskByID(int.Parse(sdr["SubtaskID"].ToString()));
                        sub.Order = int.Parse(sdr["OrderNumber"].ToString());

                        subtasks.Add(sub);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return subtasks;
        }
        #endregion

        #region INSERTS & UPDATES
        public static string insSubtask(Subtask Subtask)
        {
            string id = "";

            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.insSubtask";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("Title", Subtask.Title);
                    sqlcmd.Parameters.AddWithValue("Description", Subtask.Title);
                    sqlcmd.Parameters.AddWithValue("JobDescriptionID", Subtask.JobDescriptionID);
                    sqlcmd.Parameters.AddWithValue("CreatedBy", Subtask.CreatedBy);
                    sqlcmd.Parameters.AddWithValue("Created", DateTime.Now);
                    sqlcmd.Parameters.AddWithValue("Modified", DateTime.Now);
                    sqlcmd.Parameters.AddWithValue("ModifiedBy", Subtask.ModifiedBy);

                    id = sqlcmd.ExecuteScalar().ToString();

                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {

                }
            }//end sql

            return id;
        }

        public static void updSubtask(Subtask Subtask)
        {

            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.updSubtask";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("ID", Subtask.ID);
                    //sqlcmd.Parameters.AddWithValue("Description", Subtask.Title);
                    sqlcmd.Parameters.AddWithValue("JobDescriptionID", Subtask.JobDescriptionID);
                    sqlcmd.Parameters.AddWithValue("Modified", DateTime.Now);
                    sqlcmd.Parameters.AddWithValue("ModifiedBy", Subtask.ModifiedBy);

                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {

                }
            }//end sql
        }

        public static string insSubtask_Lookup(int JobTaskID, int SubtaskID, int Order, string CreatedBy, DateTime Created)
        {
            string id = "";

            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.insSubtask_Lookup";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("JobTaskID", JobTaskID);
                    sqlcmd.Parameters.AddWithValue("SubtaskID", SubtaskID);
                    sqlcmd.Parameters.AddWithValue("OrderNumber", Order);
                    sqlcmd.Parameters.AddWithValue("Created", Created);
                    sqlcmd.Parameters.AddWithValue("CreatedBy", CreatedBy);

                    id = sqlcmd.ExecuteScalar().ToString();

                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {

                }
            }//end sql

            return id;
        }

        #endregion

        #region DELETES
        public static void delSubtask(int ID)
        {
            int i = 0;
            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.delSubtask";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("id", ID);
                    i = sqlcmd.ExecuteNonQuery();
                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {

                }
            }//end sql
        }

        public static void delSubtask_Lookup(JobTasks JobTask)
        {
            int i = 0;
            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.delSubtask_Lookup";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("ID", JobTask.ID);
                    sqlcmd.ExecuteNonQuery();
                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {

                }
            }//end sql
        }

        public static void delSubtask_Lookup_BySubtask(Subtask Subtask)
        {
            int i = 0;
            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.delSubtask_Lookup_BySubtask";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("ID", Subtask.ID);
                    sqlcmd.ExecuteNonQuery();
                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {

                }
            }//end sql
        }
        #endregion
    }
}
