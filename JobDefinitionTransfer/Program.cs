﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Entity;
using System.Net;


namespace JobDefinitionTransfer
{
    public class Program
    {
        private static Operations_QAEntities db = new Operations_QAEntities();
        static void Main(string[] args)
        {
            getList();
            Console.Write("");
        }

        public static void getList()
        {
            Ops.OperationsDataContext odc = new Ops.OperationsDataContext(new Uri("https://portal.bh.local/sites/Ops/_vti_bin/ListData.svc"));
            odc.Credentials = System.Net.CredentialCache.DefaultCredentials;

            var definitions = odc.JobDefinition.ToList();
            
            foreach (var def in definitions)
            {
                Console.WriteLine("Title: " + def.Title);
                string details = def.Details;//.Replace("&#160;", "");
                Console.WriteLine(details);
                
                var newdef = (from x in db.JobDescriptions where x.Title == def.Title select x).FirstOrDefault();
                //var newdef = (from x in db.JobDescriptions where x.Title == "Test" select x).FirstOrDefault();

                if (newdef != null)
                {
                    newdef.Description = details;

                    newdef.Modified = DateTime.Now;
                    newdef.ModifiedBy = "MSTIL003";
                    newdef.Description = details;

                    //JobDescription jobDes = db.JobDescriptions.Find(newdef.ID);

                    //JobDescription jobdef = new JobDescription
                    //{
                    //    ID = newdef.ID,
                    //    Description = details,
                    //    Modified = DateTime.Now,
                    //    ModifiedBy = "MSTIL003"
                    //};

                    db.JobDescriptions.Attach(newdef);
                    var d = db.Entry(newdef);
                    d.Property(e => e.Modified).IsModified = true;
                    d.Property(e => e.ModifiedBy).IsModified = true;
                    d.Property(e => e.Description).IsModified = true;
                    db.SaveChanges();
                }
            } 
        }
    }
}
