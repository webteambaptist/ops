﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPSTasksAPI.Models
{
    class Recurrence
    {
        public int ID { get; set; }
        public int JobTaskID { get; set; }
        public DateTime DueDate { get; set; }
        public string FactorNameData { get; set; }
        public DateTime Started { get; set; }
        public string StartedBy { get; set; }
        public DateTime Finished { get; set; }
        public string FinishedBy { get; set; }
        public Boolean Completed { get; set; }
        public string Comments { get; set; }
        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public JobTasks JobTask { get; set; }
        public Boolean IsActive { get; set; }
        public DateTime Modified { get; set; }
        public string ModifiedBy { get; set; }

        #region SELECTS
        public static Recurrence selRecurrenceByID(int id)
        {
            Recurrence recurrence = new Recurrence();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selJobTask_RecurrenceByID", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("ID", id);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        recurrence.ID = id;
                        recurrence.JobTaskID = int.Parse(sdr["TaskID"].ToString());
                        recurrence.DueDate = DateTime.Parse(sdr["DueDate"].ToString());
                        recurrence.FactorNameData = sdr["FactorNameData"].ToString();
                        recurrence.Started = DateTime.Parse(sdr["Started"].ToString());
                        recurrence.StartedBy = sdr["StartedBy"].ToString();
                        recurrence.Finished = DateTime.Parse(sdr["Finished"].ToString());
                        recurrence.FinishedBy = sdr["FinsihedBy"].ToString();
                        recurrence.Completed = Boolean.Parse(sdr["Completed"].ToString());
                        recurrence.Comments = sdr["Comments"].ToString();
                        recurrence.IsActive = Boolean.Parse(sdr["IsActive"].ToString());
                        recurrence.Created = DateTime.Parse(sdr["Created"].ToString());
                        recurrence.CreatedBy = sdr["CreatedBy"].ToString();
                        recurrence.ModifiedBy = sdr["ModifiedBy"].ToString();
                        try
                        {
                            recurrence.Modified = DateTime.Parse(sdr["Modified"].ToString());
                        }
                        catch { }

                        recurrence.JobTask = JobTasks.selJobTask(recurrence.JobTaskID);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return recurrence;
        }

        public static Recurrence selRecurrence_ByTaskID(int TaskID)
        {
            Recurrence recurrence = new Recurrence();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selJobTask_Recurrence_ByTaskID", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("ID", TaskID);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        recurrence.ID = int.Parse(sdr["ID"].ToString());
                        recurrence.JobTaskID = TaskID;
                        recurrence.FactorNameData = sdr["FactorNameData"].ToString();
                        recurrence.StartedBy = sdr["StartedBy"].ToString();
                        recurrence.FinishedBy = sdr["FinsihedBy"].ToString();
                        recurrence.Comments = sdr["Comments"].ToString();
                        recurrence.CreatedBy = sdr["CreatedBy"].ToString();
                        recurrence.ModifiedBy = sdr["ModifiedBy"].ToString();

                        try
                        {
                            recurrence.DueDate = DateTime.Parse(sdr["DueDate"].ToString());
                        }
                        catch { }
                        try
                        {
                            recurrence.Started = DateTime.Parse(sdr["Started"].ToString());

                        }
                        catch { }
                        try
                        {
                            recurrence.Finished = DateTime.Parse(sdr["Finished"].ToString());
                        } catch { }
                        try
                        {
                            recurrence.Completed = Boolean.Parse(sdr["Completed"].ToString());
                        }
                        catch { recurrence.Completed = false; }
                        try
                        {
                            recurrence.IsActive = Boolean.Parse(sdr["IsActive"].ToString());
                        }
                        catch { recurrence.IsActive = true; }
                        try
                        {
                            recurrence.Created = DateTime.Parse(sdr["Created"].ToString());
                        }
                        catch { }
                        try
                        {
                            recurrence.Modified = DateTime.Parse(sdr["Modified"].ToString());
                        }
                        catch { }

                        recurrence.JobTask = JobTasks.selJobTask(TaskID);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return recurrence;
        }

        public static List<Recurrence> selRecurrences()
        {
            List<Recurrence> recurrences = new List<Recurrence>();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selJobTask_Recurrence", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        Recurrence recurrence = new Recurrence();

                        recurrence.ID = int.Parse(sdr["ID"].ToString());
                        recurrence.JobTaskID = int.Parse(sdr["TaskID"].ToString());
                        recurrence.FactorNameData = sdr["FactorNameData"].ToString();
                        recurrence.StartedBy = sdr["StartedBy"].ToString();
                        recurrence.FinishedBy = sdr["FinishedBy"].ToString();
                        recurrence.Comments = sdr["Comments"].ToString();
                        recurrence.CreatedBy = sdr["CreatedBy"].ToString();
                        recurrence.ModifiedBy = sdr["ModifiedBy"].ToString();

                        try
                        {
                            recurrence.DueDate = DateTime.Parse(sdr["DueDate"].ToString());
                        } catch { }
                        try
                        {
                            recurrence.Started = DateTime.Parse(sdr["Started"].ToString());

                        } catch { }
                        try
                        {
                            recurrence.Finished = DateTime.Parse(sdr["Finished"].ToString());
                        } catch { }
                        try
                        {
                            recurrence.Completed = Boolean.Parse(sdr["Completed"].ToString());
                        }catch { recurrence.Completed = false; }
                        try
                        {
                            recurrence.IsActive = Boolean.Parse(sdr["IsActive"].ToString());
                        } catch { recurrence.IsActive = true;  }
                        try
                        {
                            recurrence.Created = DateTime.Parse(sdr["Created"].ToString());
                        } catch { }
                        try
                        {
                            recurrence.Modified = DateTime.Parse(sdr["Modified"].ToString());
                        }
                        catch { }

                        recurrence.JobTask = JobTasks.selJobTask(recurrence.JobTaskID);

                        recurrences.Add(recurrence);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return recurrences;
        }
        #endregion

        #region INSERTS & UPDATES
        public static string insRecurrence(Recurrence Recurrence)
        {
            string id = "";

            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.insJobTask_Recurrence";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("TaskID", Recurrence.JobTaskID);
                    sqlcmd.Parameters.AddWithValue("DueDate", Recurrence.DueDate);
                    sqlcmd.Parameters.AddWithValue("FactorNameData", Recurrence.FactorNameData);
                    //sqlcmd.Parameters.AddWithValue("Started", Recurrence.Started);
                    //sqlcmd.Parameters.AddWithValue("StartedBy", Recurrence.StartedBy);
                    //sqlcmd.Parameters.AddWithValue("Finished", Recurrence.Finished);
                    //sqlcmd.Parameters.AddWithValue("FinishedBy", Recurrence.FinishedBy);
                    sqlcmd.Parameters.AddWithValue("Completed", Recurrence.Completed);
                    sqlcmd.Parameters.AddWithValue("Comments", Recurrence.Comments);
                    sqlcmd.Parameters.AddWithValue("IsActive", Recurrence.IsActive);
                    sqlcmd.Parameters.AddWithValue("Created", DateTime.Now);
                    sqlcmd.Parameters.AddWithValue("CreatedBy", Recurrence.CreatedBy);
                    sqlcmd.Parameters.AddWithValue("Modified", DateTime.Now);
                    sqlcmd.Parameters.AddWithValue("ModifiedBy", Recurrence.ModifiedBy);

                    id = sqlcmd.ExecuteScalar().ToString();

                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {

                }
            }//end sql

            return id;
        }

        public static void updRecurrence(Recurrence Recurrence)
        {
            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.updRecurrence";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("ID", Recurrence.ID);
                    sqlcmd.Parameters.AddWithValue("TaskID", Recurrence.JobTaskID);
                    sqlcmd.Parameters.AddWithValue("DueDate", Recurrence.DueDate);
                    sqlcmd.Parameters.AddWithValue("FactorNameData", Recurrence.FactorNameData);
                    sqlcmd.Parameters.AddWithValue("Started", Recurrence.Started);
                    sqlcmd.Parameters.AddWithValue("StartedBy", Recurrence.StartedBy);
                    sqlcmd.Parameters.AddWithValue("Finished", Recurrence.Finished);
                    sqlcmd.Parameters.AddWithValue("FinishedBy", Recurrence.FinishedBy);
                    sqlcmd.Parameters.AddWithValue("Completed", Recurrence.Completed);
                    sqlcmd.Parameters.AddWithValue("Comments", Recurrence.Comments);
                    sqlcmd.Parameters.AddWithValue("IsActive", Recurrence.IsActive);
                    sqlcmd.Parameters.AddWithValue("Modified", DateTime.Now);
                    sqlcmd.Parameters.AddWithValue("ModifiedBy", Recurrence.ModifiedBy);

                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {

                }
            }//end sql
        }
        #endregion

        #region DELETES
        public static void delRecurrence(Recurrence Recurrence)
        {
            int i = 0;
            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.delJobTask_Recurrence";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("ID", Recurrence.ID);
                    i = sqlcmd.ExecuteNonQuery();
                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {

                }
            }//end sql
        }
        public static void delJobTask_Recurrence_ByTaskID(Recurrence Recurrence)
        {
            int i = 0;
            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.delJobTask_Recurrence_ByTaskID";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("ID", Recurrence.JobTaskID);
                    i = sqlcmd.ExecuteNonQuery();
                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {

                }
            }//end sql
        }

        public static void delJobTask_Recurrence_Week()
        {
            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.delJobTask_Recurrence_Week";
                    sqlcmd.Connection.Open();
                    sqlcmd.ExecuteNonQuery();
                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {

                }
                //end sql
            }
        }
        #endregion
    }
}
