﻿using OPSTasksAPI.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.UI;

namespace OPSTasksAPI.Controllers
{
    /// <summary>
    /// Task scheduler API
    /// </summary>
    
    [RoutePrefix("api/task")]
    public class TaskController : ApiController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// updates schedule
        /// </summary>
        /// <param name="date"></param>
        [Route("update")]
        [HttpGet]
        public IHttpActionResult startUpdate()
        {
            try
            {
                DateTime date = DateTime.Now;

                    getTasks(date);
                    date = date.AddDays(1);
                //for (int i = 0; i <= 7; i++)
                //{
                //    getTasks(date);
                //    date = date.AddDays(1);
                //}

                return Ok();
            }
            catch( Exception ex)
            {
                log.Info("startUpdate()", ex.InnerException);
                return BadRequest();
            }
        }

        /// <summary>
        /// deletes tasks from week except current date.
        /// </summary>
        [Route("delete")]
        [HttpGet]
        public IHttpActionResult Delete()
        {
            Recurrence.delJobTask_Recurrence_Week();
            return Ok();
        }

        /// <summary>
        /// method that handles refreshing the tasks using Hangfire.
        /// </summary>
        public void RefreshOPSDB()
        {
            DateTime date = DateTime.Now;

            for (int i = 0; i <= 7; i++)
            {
                getTasks(date);
                date = date.AddDays(1);
            }
        }

        protected static void getTasks(DateTime date)
        {
            try
            {
                List<JobTasks> jobs = JobTasks.selJobTasks();
                List<Recurrence> recs = Recurrence.selRecurrences();
                List<int> ReplaceTasks = new List<int>();
                DateTime currDate = date;

                foreach (JobTasks job in jobs)
                {

                    Recurrence jobrec = (from x in recs where x.JobTaskID == job.ID && x.DueDate.Date == currDate.Date select x).FirstOrDefault();
                    JobTasks jobtask = (from x in jobs where jobrec != null && x.ID == jobrec.JobTaskID select x).FirstOrDefault();

                    if (jobtask != null && jobtask.ReplaceTaskID > 0)
                    {
                        ReplaceTasks.Add(jobtask.ReplaceTaskID);
                    }

                    if (jobrec == null)
                    {
                        if (occursToday(job, currDate) && job.EndDate.Date >= date.Date)
                        {
                            addRecurrence(ReplaceTasks, job, currDate);
                        }
                    }
                    //else
                    //{
                    //    if (occursToday(job, currDate) && job.Modified > jobrec.Modified)
                    //    {
                    //        var newTime = DateTime.Parse(currDate.Month + "/" + currDate.Day + "/" + currDate.Year + " " + job.StartDate.Hour + ":" + job.StartDate.Minute + job.StartDate.ToString("tt"));

                    //        jobrec.ModifiedBy = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Substring(3).ToUpper();
                    //        jobrec.DueDate = newTime;

                    //        Recurrence.updRecurrence(jobrec);
                    //    }
                    //    else if (!occursToday(job, currDate))
                    //    {

                    //    }
                    //}               
                }

                removeReplacedTasks(ReplaceTasks, jobs, currDate);
            }
            catch (Exception ex)
            {
                log.Info("getTasks", ex.InnerException);
            }
        }

        private static void addRecurrence(List<int> ReplaceTasks, JobTasks job, DateTime currDate)
        {
            try
            {
                var newTime = DateTime.Parse(currDate.Month + "/" + currDate.Day + "/" + currDate.Year + " " + job.StartDate.Hour + ":" + job.StartDate.Minute + job.StartDate.ToString("tt"));

                Recurrence rec = new Recurrence();
                rec.JobTaskID = job.ID;
                rec.IsActive = true;
                rec.Created = DateTime.Now;
                rec.CreatedBy = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Substring(3).ToUpper();
                rec.Modified = DateTime.Now;
                rec.ModifiedBy = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Substring(3).ToUpper();
                rec.DueDate = newTime;
                rec.FactorNameData = "";
                rec.Comments = "";
                rec.Completed = false;
                string id = Recurrence.insRecurrence(rec);

                if (job.ReplaceTaskID > 0)
                {
                    ReplaceTasks.Add(job.ReplaceTaskID);
                }

                if (job.HasSubtasks)
                {
                    List<Subtask> subs = Subtask.selSubtask_Lookup(job.ID);

                    if (subs.Count > 0)
                    {
                        foreach (Subtask sub in subs)
                        {
                            addSubtasks(job, currDate, rec, subs, sub);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Info("addRecurrence", ex.InnerException);
            }
        }

        private static void addSubtasks(JobTasks job, DateTime currDate, Recurrence rec, List<Subtask> subs, Subtask sub)
        {
            Recurrence_Subtask recsub = new Recurrence_Subtask();

            try
            {
                recsub.SubtaskID = sub.ID;
                recsub.ParentID = job.ID;
                recsub.Completed = false;
                recsub.Comments = "";
                recsub.Created = DateTime.Now;
                recsub.CreatedBy = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Substring(3).ToUpper();
                recsub.Modified = DateTime.Now;
                recsub.ModifiedBy = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Substring(3).ToUpper();
                recsub.DueDate = rec.DueDate;
                recsub.FactorNameData = "";

                //if (sub.Order == 1)
                //{
                //    DateTime endDate = DateTime.Parse(job.StartDate.ToString()).ToLocalTime();
                //    DateTime fieldTime = DateTime.Parse(currDate.Month + "/" + currDate.Day + "/" + currDate.Year + " " + endDate.Hour + ":" + endDate.Minute + endDate.ToString("tt"));

                //    recsub.DueDate = fieldTime;
                //}

                Recurrence_Subtask.insRecurrence_Subtask(recsub);
            }
            catch (Exception ex)
            {
                log.Info("addSubtasks", ex.InnerException);
            }
        }

        protected static Boolean occursToday(JobTasks job, DateTime date)
        {
            Boolean occursToday = false;

            try
            {

                string day = (from d in job.DaysOfWeek where d == date.DayOfWeek.ToString() select d).FirstOrDefault();


                if (job.Frequency.ToLower() == "daily")
                {
                    //Occurs every (x) amount of days
                    if ((date.DayOfYear - job.StartDate.DayOfYear) % job.Intervals == 0)
                    {
                        occursToday = true;
                    }
                }
                else if (job.Frequency.ToLower() == "weekly")
                {
                    //Occurs every (x) amount of weeks on (day of week)
                    if (!string.IsNullOrEmpty(day))
                    {
                        int weekNumb = GetWeekNumber(date);
                        int startWeekNumb = GetWeekNumber(job.StartDate);

                        if ((weekNumb - startWeekNumb) % job.Intervals == 0)
                        {
                            occursToday = true;
                        }
                    }
                }
                else if (job.Frequency.ToLower() == "monthly")
                {
                    int dayOfMonth = job.Day;
                    string place = job.Placement;
                    day = (from d in job.DaysOfWeek where d == date.DayOfWeek.ToString() || d == "Day" || d == "Weekday" || d == "Weekend Day" select d).FirstOrDefault();

                    if (dayOfMonth > 0)
                    {
                        //Occurs every (x) amount of months on day (y) of month
                        if (date.Day == dayOfMonth && ((date.Month - job.StartDate.Month) % job.Intervals == 0))
                        {
                            occursToday = true;
                        }
                    }
                    else if (!string.IsNullOrEmpty(day))
                    {
                        //Occurs every (x) amount of months on (1st, 2nd, ..., last) of the month on (day of week)

                        if ((date.Month - job.StartDate.Month) % job.Intervals == 0)
                        {
                            occursToday = getMonthly(date, job); //CheckSpecificDayOfMonth(place, date, day);
                        }
                    }
                }
                else if (job.Frequency.ToLower() == "quarterly")
                {
                    int dayOfMonth = job.Day;
                    string month = job.Month;

                    string mn = date.Month.ToString("00");
                    string dy = date.Day.ToString("00");

                    if (mn == "01" && dy == "01" || mn == "04" && dy == "01" || mn == "07" && dy == "01" || mn == "10" && dy == "01")
                    {
                        occursToday = true;
                    }
                    else
                    {

                    }
                }
                else if (job.Frequency.ToLower() == "half year")
                {
                    int dayOfMonth = job.Day;
                    string month = job.Month;

                    string mn = date.Month.ToString("00");
                    string dy = date.Day.ToString("00");

                    if (mn == "01" && dy == "01" || mn == "07" && dy == "01")
                    {
                        occursToday = true;
                    }
                    else
                    {

                    }
                }
                else if (job.Frequency.ToLower() == "yearly")
                {

                    int dayOfMonth = job.Day;
                    string month = job.Month;
                    day = (from d in job.DaysOfWeek where d == date.DayOfWeek.ToString() || d == "Day" || d == "Weekday" || d == "Weekend Day" select d).FirstOrDefault();

                    if (dayOfMonth > 0 && dayOfMonth == date.Day)
                    {
                        //Occurs every year on (month)/(day)
                        if (month == date.Month.ToString("MMMM") && day == date.DayOfWeek.ToString())
                        {
                            occursToday = true;
                        }
                    }
                    else
                    {
                        //Occurs every year on the (1st, 2nd, ..., last) (day of week) of (month)
                        string place = "";
                        int dayOfWeek = job.Day;

                        //if (month == date.Month.ToString("MMMM") && day == date.DayOfWeek.ToString())
                        //{
                        occursToday = getMonthly(date, job);//CheckSpecificDayOfMonth(place, date, day);
                                                            //}
                    }
                }
            }
            catch (Exception ex)
            {
                log.Info("occursToday", ex.InnerException);
            }
            return occursToday;
        }

        public static int GetWeekNumber(DateTime date)
        {
            //returns the week number of date

            int numb = 0;
            try
            { 
                var cultureInfo = System.Globalization.CultureInfo.CurrentCulture;
                numb = cultureInfo.Calendar.GetWeekOfYear(date, cultureInfo.DateTimeFormat.CalendarWeekRule, cultureInfo.DateTimeFormat.FirstDayOfWeek);
            }
            catch (Exception ex)
            {
                log.Info("GetWeekNumber", ex.InnerException);
            }

            return numb;
        }

        public static void removeReplacedTasks(List<int> IDs, List<JobTasks> jobs, DateTime date)
        {
            try
            { 
                List<Recurrence> recs = Recurrence.selRecurrences();
                if (IDs.Count > 0)
                {
                    foreach (int id in IDs)
                    {
                        Recurrence rec = (from x in recs where x.JobTaskID == id && x.DueDate.Date == date.Date select x).FirstOrDefault();
                        JobTasks job = (from x in jobs where x.ID == id select x).FirstOrDefault();

                        if (rec != null)
                        {
                            Recurrence.delRecurrence(rec);

                            if (job.HasSubtasks)
                            {
                                foreach (Subtask sub in job.Subtasks)
                                {
                                    Recurrence_Subtask.delRecurrence_Subtask(sub.ID);
                                }
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Info("removeReplacedTasks", ex.InnerException);
            }
        }

        public static bool CheckSpecificDayOfMonth(string weekDayOfMonth, DateTime date, string day)
        {
            //checks of event occurs on first sunday of month, fourth wed, etc.

            bool occurs = false;

            try
            { 
                int weekdaynumb = 0;
                int currentWeekdayNumb = (int)date.DayOfWeek;

                if (weekDayOfMonth == "first")
                {
                    weekdaynumb = 1;
                }
                else if (weekDayOfMonth == "second")
                {
                    weekdaynumb = 2;
                }
                else if (weekDayOfMonth == "third")
                {
                    weekdaynumb = 3;
                }
                else if (weekDayOfMonth == "fourth")
                {
                    weekdaynumb = 4;
                }
                else if (weekDayOfMonth == "last")
                {
                    //if its last weekday in a month, itll be greather than number of days in month - 7
                    var daysinmonth = DateTime.DaysInMonth(date.Year, date.Month);

                    if (date.Day > daysinmonth - 7)
                    {
                        occurs = true;
                    }
                }

                var month = date.Month;
                var firstofmonth = DateTime.Parse(date.Month + "/1/" + date.Year + " 12:00 AM");

                //goes thru the first of the month to current date
                while (firstofmonth <= date)
                {
                    occurs = CheckDayOfWeek(day, currentWeekdayNumb, weekdaynumb);

                    firstofmonth = firstofmonth.AddDays(1);
                    currentWeekdayNumb++;
                }
            }
            catch (Exception ex)
            {
                log.Info("CheckSpecificDayOfMonth", ex.InnerException);
            }

            return occurs;
        }

        public static bool CheckDayOfWeek(string day, int currWeekdayNum, int weekdaynumb)
        {
            //checks if event is supposed to occur on this day of the week

            bool occurs = false;

            try
            {
                int currentDayOfWeek = (int)DateTime.Now.DayOfWeek;

                //first it checks if what day of week is in RecurrenceData and if that day is today
                if (day.ToLower() == "sunday" && currentDayOfWeek == 0)
                {
                    if (currWeekdayNum == weekdaynumb)
                    {
                        occurs = true;
                    }
                }
                else if (day.ToLower() == "monday" && currentDayOfWeek == 1)
                {
                    if (currWeekdayNum == weekdaynumb)
                    {
                        occurs = true;
                    }
                }
                else if (day.ToLower() == "tuesday" && currentDayOfWeek == 2)
                {
                    if (currWeekdayNum == weekdaynumb)
                    {
                        occurs = true;
                    }
                }
                else if (day.ToLower() == "wednesday" && currentDayOfWeek == 3)
                {
                    if (currWeekdayNum == weekdaynumb)
                    {
                        occurs = true;
                    }
                }
                else if (day.ToLower() == "thursday" && currentDayOfWeek == 4)
                {
                    if (currWeekdayNum == weekdaynumb)
                    {
                        occurs = true;
                    }
                }
                else if (day.ToLower() == "friday" && currentDayOfWeek == 5)
                {
                    if (currWeekdayNum == weekdaynumb)
                    {
                        occurs = true;
                    }
                }
                else if (day.ToLower() == "saturday" && currentDayOfWeek == 6)
                {
                    if (currWeekdayNum == weekdaynumb)
                    {
                        occurs = true;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Info("CheckDayOfWeek", ex.InnerException);
            }

            return occurs;
        }

        public static Boolean getMonthly(DateTime date, JobTasks job)
        {
            Boolean occurs = false;

            try
            { 
            int daysinmonth = DateTime.DaysInMonth(date.Year, date.Month);


            int month = 0;

            if (string.IsNullOrEmpty(job.Month))
            {
                month = date.Month;
            }
            else
            {
                month = DateTime.Parse(job.Month + "/01/2000").Month;
            }

            string day = (from d in job.DaysOfWeek where d == date.DayOfWeek.ToString() || d == "Day" || d == "Weekday" || d == "Weekend Day" select d).FirstOrDefault();


            string place = job.Placement;

            if (!string.IsNullOrEmpty(day))
            {

                if (date.Month == month)
                {
                    int count = 0;

                    if (day == "Day")
                    {
                        if (place == "First" && date.Day == 1)
                        {
                            occurs = true;
                        }
                        else if (place == "Second" && date.Day == 2)
                        {
                            occurs = true;
                        }
                        else if (place == "Third" && date.Day == 3)
                        {
                            occurs = true;
                        }
                        else if (place == "Fourth" && date.Day == 4)
                        {
                            occurs = true;
                        }
                        else if (place == "Last" && date.Day == daysinmonth)
                        {
                            occurs = true;
                        }

                    }
                    else
                    {

                        for (int i = 1; i <= daysinmonth; i++)
                        {
                            DateTime temp = DateTime.Parse(date.Month + "/" + i + "/" + date.Year);

                            if (temp.DayOfWeek.ToString().ToLower() == day.ToLower())
                            {
                                count = count + 1;

                                if (count == 1 && place == "First" && date.Day == i)
                                {
                                    occurs = true;
                                    break;
                                }
                                else if (count == 2 && place == "Second" && date.Day == i)
                                {
                                    occurs = true;
                                    break;
                                }
                                else if (count == 3 && place == "Third" && date.Day == i)
                                {
                                    occurs = true;
                                    break;
                                }
                                else if (count == 4 && place == "Fourth" && date.Day == i)
                                {
                                    occurs = true;
                                    break;
                                }
                                else if (count > 1 && place == "Last" && date.Day == i && date.AddDays(7).Month > temp.Month)
                                {
                                    occurs = true;
                                    break;
                                }
                            }
                            //else 
                            else if (day == "Weekday")
                            {
                                if (temp.DayOfWeek != DayOfWeek.Saturday && temp.DayOfWeek != DayOfWeek.Sunday)
                                {
                                    count = count + 1;

                                    if (count == 1 && place == "First" && date.Day == i)
                                    {
                                        occurs = true;
                                        break;
                                    }
                                    else if (count == 2 && place == "Second" && date.Day == i)
                                    {
                                        occurs = true;
                                        break;
                                    }
                                    else if (count == 3 && place == "Third" && date.Day == i)
                                    {
                                        occurs = true;
                                        break;
                                    }
                                    else if (count == 4 && place == "Fourth" && date.Day == i)
                                    {
                                        occurs = true;
                                        break;
                                    }
                                    else if (place == "Last" && date.Day == i)
                                    {
                                        if (temp.DayOfWeek == DayOfWeek.Friday && i + 3 > daysinmonth)
                                        {
                                            occurs = true;
                                            break;
                                        }
                                        else if (i + 1 > daysinmonth)
                                        {
                                            occurs = true;
                                            break;
                                        }
                                    }
                                }

                            }
                            else if (day == "Weekend Day")
                            {
                                if (temp.DayOfWeek == DayOfWeek.Saturday || temp.DayOfWeek == DayOfWeek.Sunday)
                                {
                                    count = count + 1;
                                    if (count == 1 && place == "First" && date.Day == i)
                                    {
                                        occurs = true;
                                        break;
                                    }
                                    else if (count == 2 && place == "Second" && date.Day == i)
                                    {
                                        occurs = true;
                                        break;
                                    }
                                    else if (count == 3 && place == "Third" && date.Day == i)
                                    {
                                        occurs = true;
                                        break;
                                    }
                                    else if (count == 4 && place == "Fourth" && date.Day == i)
                                    {
                                        occurs = true;
                                        break;
                                    }
                                    else if (place == "Last" && date.Day == i)
                                    {
                                        if (temp.DayOfWeek == DayOfWeek.Sunday && (i + 6 > daysinmonth))
                                        {
                                            occurs = true;
                                            break;
                                        }
                                        else if (temp.DayOfWeek == DayOfWeek.Saturday && (i + 7 > daysinmonth))
                                        {
                                            occurs = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            }
            catch (Exception ex)
            {
                log.Info("getMonthly", ex.InnerException);
            }

            return occurs;
        }
    }
}