﻿using Hangfire;
using Microsoft.Owin;
using OPSTasksAPI.Controllers;
using Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;


[assembly: OwinStartup(typeof(OPSTasksAPI.App_Start.Startup))]

namespace OPSTasksAPI.App_Start
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            
            Hangfire.GlobalConfiguration.Configuration.UseSqlServerStorage(ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString);
            app.UseHangfireDashboard();
            app.UseHangfireServer();
            //RecurringJob.AddOrUpdate(() => UpdateOPS(), Cron.Hourly());

        }
        //public static void UpdateOPS()
        //{
        //    TaskController controller = new TaskController();
        //    controller.RefreshOPSDB();
        //}
    }
}