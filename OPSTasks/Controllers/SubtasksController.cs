﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using OPSTasks.Models;
using OPSTasks.Helpers;

namespace OPSTasks.Controllers
{
    public class SubtasksController : Controller
    {
        private Entities db = new Entities();

        // GET: Subtasks
        public ActionResult Index()
        {
            var userName = User.Identity.Name;
            var groups = UserSettings.GetGroups(userName);
            var userIsAdmin = groups.Where(x => x.Contains("OPS Admins"));
            var userIsUser = groups.Where(x => x.Contains("OPS Users"));

            var subtasks = (from s in db.Subtasks
                           from r in db.JobDescriptions.Where(x => x.ID == s.JobDescriptionID).DefaultIfEmpty()
                           select new SubtaskViewModel
                           {
                               Created = s.Created,
                               CreatedBy = s.CreatedBy,
                               Modified = s.Modified,
                               ModifiedBy = s.ModifiedBy,
                               JobDescription = r.Title,
                               ID = s.ID,
                               JobDescriptionID = s.JobDescriptionID,
                               JobDescriptionURL = s.Title,
                               Title = s.Title
                           }).OrderBy(x => x.Title).ToList();

            if (userIsAdmin.Any())
            {
                ViewBag.IsAdmin = true;
                ViewBag.IsUser = false;
                return View(subtasks);
            }

            if (userIsUser.Any())
            {
                ViewBag.IsAdmin = false;
                ViewBag.IsUser = true;
                return View(subtasks);
            }

            return RedirectToAction("Index", "UnauthorizedAccess");            
        }

        // GET: Subtasks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var subtask = (from s in db.Subtasks.Where(x => x.ID == id).DefaultIfEmpty()
                    from r in db.JobDescriptions.Where(x => x.ID == s.JobDescriptionID).DefaultIfEmpty()
                select new SubtaskViewModel
                {
                    Created = s.Created,
                    CreatedBy = s.CreatedBy,
                    Modified = s.Modified,
                    ModifiedBy = s.ModifiedBy,
                    JobDescription = r.Title,
                    ID = s.ID,
                    JobDescriptionID = s.JobDescriptionID,
                    JobDescriptionURL = s.Title,
                    Title = s.Title
                }).FirstOrDefault(); 

            if (subtask == null)
            {
                return HttpNotFound();
            }
            return View(subtask);
        }

        // GET: Subtasks/Create
        public ActionResult Create()
        {        
            var subtask = new SubtaskViewModel();
            subtask.JobDescriptions = new List<SelectListItem>(
                                      from descriptions in db.JobDescriptions
                                      select new SelectListItem
                                      {
                                          Value = descriptions.ID.ToString(),
                                          Text = descriptions.Title
                                      }).OrderBy(x => x.Text).ToList();

            return View(subtask);
        }

        // POST: Subtasks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Title,JobDescriptionID,Created,CreatedBy")] Subtask subtask)
        {
            if (ModelState.IsValid)
            {
                subtask.Created = DateTime.Now;
                subtask.CreatedBy = User.Identity.Name.Split('\\')[1].ToUpper();
                subtask.Modified = DateTime.Now;
                subtask.ModifiedBy = User.Identity.Name.Split('\\')[1].ToUpper();
                
                db.Subtasks.Add(subtask);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(subtask);
        }

        // GET: Subtasks/Edit/5
        public ActionResult Edit(int? id)
        {       
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var subtask = (from s in db.Subtasks.Where(x => x.ID == id).DefaultIfEmpty()
                           from r in db.JobDescriptions.Where(x => x.ID == s.JobDescriptionID).DefaultIfEmpty()
                           select new SubtaskViewModel
                           {
                               Created = s.Created,
                               CreatedBy = s.CreatedBy,
                               Modified = s.Modified,
                               ModifiedBy = s.ModifiedBy,
                               JobDescription = r.Title,
                               ID = s.ID,
                               JobDescriptionID = s.JobDescriptionID,
                               JobDescriptionURL = s.Title,
                               Title = s.Title
                           }).FirstOrDefault();

            subtask.JobDescriptions = new List<SelectListItem>(
                                      from descriptions in db.JobDescriptions
                                      select new SelectListItem
                                      {
                                          Value = descriptions.ID.ToString(),
                                          Text = descriptions.Title
                                      }).OrderBy(x => x.Text).ToList();

            if (subtask == null)
            {
                return HttpNotFound();
            }
            return View(subtask);
        }

        // POST: Subtasks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Title,JobDescriptionID")] Subtask subtask)
        {
            if (ModelState.IsValid)
            {
                subtask.Modified = DateTime.Now;
                subtask.ModifiedBy = User.Identity.Name.Split('\\')[1].ToUpper();

                db.Subtasks.Attach(subtask);
                var entry = db.Entry(subtask);
                entry.Property(e => e.Modified).IsModified = true;
                entry.Property(e => e.ModifiedBy).IsModified = true;
                entry.Property(e => e.JobDescriptionID).IsModified = true;
                entry.Property(e => e.Title).IsModified = true;
                db.SaveChanges();

                //db.Entry(subtask);
                //db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(subtask);
        }

        // GET: Subtasks/Delete/5
        public ActionResult Delete(int? id)
        {    
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            var model = (from s in db.Subtasks.Where(x => x.ID == id)
                        from jd in db.JobDescriptions.Where(x => x.ID == s.JobDescriptionID).DefaultIfEmpty()
                        select new SubtaskViewModel                         
                        {
                            ID = s.ID,
                            Title = s.Title,
                            JobDescription = jd.Title
                        }).FirstOrDefault();

            if (model == null)
            {
                return HttpNotFound();
            }

            return View(model);
        }

        // POST: Subtasks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {      
            Subtask subtask = db.Subtasks.Find(id);
            var subTask_Lookup = db.Subtask_Lookup.Where(x => x.SubtaskID == id);
            var subTask_Recurrence = db.Subtask_Recurrence.Where(x => x.SubtaskID == id);
            db.Subtasks.Remove(subtask);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
