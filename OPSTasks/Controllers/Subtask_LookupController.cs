﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using OPSTasks.Models;
using OPSTasks.Models.Subtask_LookupViewModels;
using OPSTasks.Helpers;

namespace OPSTasks.Views
{
    public class Subtask_LookupController : Controller
    {
        private Entities db = new Entities();

        // GET: Subtask_Lookup        
        public ActionResult Index(int? id)
        {
            var userName = User.Identity.Name;
            var groups = UserSettings.GetGroups(userName);
            var userIsAdmin = groups.Where(x => x.Contains("OPS Admins"));
            var userIsUser = groups.Where(x => x.Contains("OPS Users"));

            ViewBag.JobTask = (from j in db.JobTasks.Where(x => x.ID == id)
                              select j.Title).FirstOrDefault();

            ViewBag.JobTaskID = id;

            var subTasks = (from st in db.Subtask_Lookup.Where(x => x.JobTaskID == id)                           
                           from j in db.JobTasks.Where(x => x.ID == st.JobTaskID).DefaultIfEmpty()
                           from s in db.Subtasks.Where(x => x.ID == st.SubtaskID).DefaultIfEmpty()
                           select new Subtask_LookupViewModel
                           {
                               JobTask = j.Title,
                               Subtask = s.Title,
                               OrderNumber = st.OrderNumber,
                               ID = st.ID,
                               Created = st.Created,
                               CreatedBy = st.CreatedBy,
                               JobTaskID = st.JobTaskID,
                               SubtaskID = st.SubtaskID
                           }).OrderBy(x => x.OrderNumber.Value);

            if (userIsAdmin.Any())
            {
                ViewBag.IsAdmin = true;
                ViewBag.IsUser = false;
                return View(subTasks);
            }

            if (userIsUser.Any())
            {
                ViewBag.IsAdmin = false;
                ViewBag.IsUser = true;
                return View(subTasks);
            }

            return RedirectToAction("Index", "UnauthorizedAccess");            
        }

        // GET: Subtask_Lookup/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subtask_Lookup subtask_Lookup = db.Subtask_Lookup.Find(id);
            if (subtask_Lookup == null)
            {
                return HttpNotFound();
            }
            return View(subtask_Lookup);
        }

        // GET: Subtask_Lookup/Create
        public ActionResult Create(int? id)
        {  
            ViewBag.JobTask = (from j in db.JobTasks.Where(x => x.ID == id)
                              select j.Title).FirstOrDefault();

            var model = (from t in db.JobTasks.Where(x => x.ID == id)
                        select new Subtask_LookupViewModel
                        {
                            JobTaskID = t.ID
                        }).FirstOrDefault();

            model.Subtasks = new List<SelectListItem>(
                             from subTasks in db.Subtasks
                             select new SelectListItem
                             {
                                 Value = subTasks.ID.ToString(),
                                 Text = subTasks.Title
                             }).OrderBy(x => x.Text).ToList();

           

            return View(model);
        }

        // POST: Subtask_Lookup/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "JobTaskID,SubtaskID,OrderNumber")] Subtask_LookupViewModel subtask_Lookup)
        {
            if (ModelState.IsValid)
            {
                var model = new Subtask_Lookup();
                model.JobTaskID = subtask_Lookup.JobTaskID;
                model.SubtaskID = subtask_Lookup.SubtaskID;
                model.OrderNumber = subtask_Lookup.OrderNumber;
                model.Created = DateTime.Now;
                model.CreatedBy = User.Identity.Name.Split('\\')[1];
                db.Subtask_Lookup.Add(model);
                db.SaveChanges();
                return RedirectToAction("Index", new { id = subtask_Lookup.JobTaskID });
            }

            return View(subtask_Lookup);
        }

        // GET: Subtask_Lookup/Edit/5
        public ActionResult Edit(int? id)
        {    
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }           

            var model = (from t in db.Subtask_Lookup.Where(x => x.ID == id)
                         from d in db.Subtasks.Where(x => x.ID == t.SubtaskID).DefaultIfEmpty()
                         select new Subtask_LookupViewModel
                         {
                             ID = t.ID,
                             JobTaskID = t.JobTaskID,
                             Subtask = d.Title
                         }).FirstOrDefault();

            if (model == null)
            {
                return HttpNotFound();
            }


            return View(model);
        }

        // POST: Subtask_Lookup/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID, OrderNumber, JobTaskID")] Subtask_LookupViewModel model)
        {
            if (ModelState.IsValid)
            {
                var modelToEdit = new Subtask_Lookup();
                modelToEdit.ID = model.ID;
                modelToEdit.OrderNumber = model.OrderNumber;
                db.Subtask_Lookup.Attach(modelToEdit);
                var entry = db.Entry(modelToEdit);
                entry.Property(e => e.OrderNumber).IsModified = true;
                db.SaveChanges();
                return RedirectToAction("Index", new { id = model.JobTaskID });
            }
            return View(model);
        }

        // GET: Subtask_Lookup/Delete/5
        public ActionResult Delete(int? id)
        {  
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subtask_Lookup subtask_Lookup = db.Subtask_Lookup.Find(id);
            if (subtask_Lookup == null)
            {
                return HttpNotFound();
            }
            return View(subtask_Lookup);
        }

        // POST: Subtask_Lookup/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        { 
            Subtask_Lookup subtask_Lookup = db.Subtask_Lookup.Find(id);
            db.Subtask_Lookup.Remove(subtask_Lookup);
            db.SaveChanges();
            return RedirectToAction("Index", new { id = subtask_Lookup.JobTaskID });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
