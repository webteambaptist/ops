﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using OPSTasks.Models;
using OPSTasks.Models.JobTaskViewModels;
using OPSTasks.Helpers;

namespace OPSTasks.Controllers
{
    public class JobTasksController : Controller
    {
        private Entities db = new Entities();

        // GET: JobTasks
        public ActionResult Index()
        {
            var userName = User.Identity.Name;
            var groups = UserSettings.GetGroups(userName);
            var userIsAdmin = groups.Where(x => x.Contains("OPS Admins"));
            var userIsUser = groups.Where(x => x.Contains("OPS Users"));           

            var jobTaskViewModel = (from j in db.JobTasks
                                   from d in db.JobDescriptions.Where(x => x.ID == j.JobDescriptionID).DefaultIfEmpty()
                                   from r in db.JobTasks.Where(x => x.ID == j.ReplaceTaskID).DefaultIfEmpty()
                                   select new JobTaskViewModel
                                   {
                                       Created = j.Created,
                                       CreatedBy = j.CreatedBy,
                                       Description = j.Description,
                                       EndDate = j.EndDate,
                                       Frequency = j.Frequency,
                                       ID = j.ID,
                                       Intervals = j.Intervals,
                                       JobDescriptionID = j.JobDescriptionID,
                                       JobDescriptionURL = d.Title,
                                       ReplaceTask = r.Title,
                                       StartDate = j.StartDate,
                                       Title = j.Title,                                       
                                       HasSubtasks = j.HasSubtasks != null ? (bool)j.HasSubtasks : false,
                                       Day = (int)j.Day,
                                       Month = j.Month,
                                       Placement = j.Placement,
                                       Modified = j.Modified,
                                       ModifiedBy = j.ModifiedBy
                                   }).OrderBy(x => x.Title).ToList();

            if (userIsAdmin.Any())
            {
                ViewBag.IsAdmin = true;
                ViewBag.IsUser = false;
                return View(jobTaskViewModel);
            }

            if (userIsUser.Any())
            {
                ViewBag.IsAdmin = false;
                ViewBag.IsUser = true;
                return View(jobTaskViewModel);
            }

            return RedirectToAction("Index", "UnauthorizedAccess");            
        }

        // GET: JobTasks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var checkedWeekDays = db.DaysOfWeek_Lookup.Where(x => x.TaskID == id).ToList();

            var model = (from j in db.JobTasks.Where(x => x.ID == id)
                        from d in db.JobDescriptions.Where(x => x.ID == j.JobDescriptionID).DefaultIfEmpty()
                        select new JobTaskViewModel
                        {
                            Created = j.Created,
                            CreatedBy = j.CreatedBy,
                            Description = j.Description,
                            ReplaceTask = j.ReplaceTaskID > 0 ? db.JobTasks.Where(x => x.ID == j.ReplaceTaskID).Select(x => x.Title).FirstOrDefault() : "None",
                            Frequency = j.Frequency,
                            ID = j.ID,
                            Intervals = j.Intervals,
                            JobDescription = d.Title,
                            Modified = j.Modified,
                            ModifiedBy = j.ModifiedBy,
                            Title = j.Title,
                            HasSubtasks = j.HasSubtasks != null ? (bool)j.HasSubtasks : false,
                            Day = (int)j.Day,
                            Month = j.Month,
                            Placement = j.Placement,
                            StartDate = j.StartDate,
                            EndDate = j.EndDate
                        }).FirstOrDefault();

            var checkBoxListItems = new List<CheckBoxList>();
            var days = db.DaysOfWeeks.ToList();
            foreach (var day in days)
            {
                bool containsDay = false;
                foreach (var checkedDay in checkedWeekDays)
                {
                    if (checkedDay.DayOfWeek == day.Day)
                    {
                        containsDay = true;
                        break;
                    }
                }
                if (containsDay)
                {
                    checkBoxListItems.Add(new CheckBoxList()
                    {
                        text = day.Day,
                        id = day.ID,
                        isChecked = true                                                
                    });
                }
                else
                {
                    checkBoxListItems.Add(new CheckBoxList()
                    {
                        text = day.Day,
                        id = day.ID,
                        isChecked = false
                    });
                }

            }
            model.DaysOfWeek = checkBoxListItems;

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // GET: JobTasks/Create
        public ActionResult Create()
        {              
            var model = new JobTaskViewModel();
            
            List<SelectListItem> frequencyListItems = new List<SelectListItem>();
            string[] frecuencies = { "Daily", "Weekly", "Monthly", "Quarterly", "Half Year", "Yearly" };
            foreach (var item in frecuencies)
            {
                frequencyListItems.Add(new SelectListItem
                {
                    Text = item,
                    Value = item
                });
            }

            List<SelectListItem> MonthsListItems = new List<SelectListItem>();
            string[] months = { "None", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
            foreach (var item in months)
            {
                MonthsListItems.Add(new SelectListItem
                {
                    Text = item,
                    Value = item
                });
            }

            List<SelectListItem> PlaceListItems = new List<SelectListItem>();
            string[] placement = { "None", "First", "Second", "Third", "Fourth",  "Last" };
            foreach (var item in placement)
            {
                PlaceListItems.Add(new SelectListItem
                {
                    Text = item,
                    Value = item
                });
            }
            
            model.Frequencies = frequencyListItems;
            model.Months = MonthsListItems;
            model.Placements = PlaceListItems;

            model.JobDescriptions = new List<SelectListItem>(
                                    from descriptions in db.JobDescriptions
                                    select new SelectListItem
                                    {
                                        Value = descriptions.ID.ToString(),
                                        Text = descriptions.Title
                                    }).OrderBy(x => x.Text).ToList();

            var checkBoxListItems = new List<CheckBoxList>();
            var days = db.DaysOfWeeks;
            foreach (var day in days)
            {
                checkBoxListItems.Add(new CheckBoxList()
                {
                    text = day.Day,                    
                    id = day.ID
                });
            }
            model.DaysOfWeek = checkBoxListItems;

            model.ReplacementTasks = new List<SelectListItem>(
                                     from replaceTask in db.JobTasks
                                     select new SelectListItem
                                     {
                                         Value = replaceTask.ID.ToString(),
                                         Text = replaceTask.Title
                                     }).OrderBy(x => x.Text).ToList(); ;

            return View(model);
        }

        // POST: JobTasks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Description, EndDate, Frequency, Description, EndDate, Frequency, HasSubtasks, ID, Intervals, JobDescriptionID, ReplaceTaskID, StartDate, Title, DaysOfWeek, Placement, Month, byDate")] JobTaskViewModel model)
        {
            if (ModelState.IsValid)
            {                
                var modelToAdd = new JobTask();
                modelToAdd.Created = DateTime.Now;
                modelToAdd.CreatedBy = User.Identity.Name.Split('\\')[1];
                modelToAdd.Modified = DateTime.Now;
                modelToAdd.ModifiedBy = User.Identity.Name.Split('\\')[1];
                modelToAdd.Description = model.Description;
                modelToAdd.EndDate = model.EndDate;
                modelToAdd.Frequency = model.Frequency;
                modelToAdd.HasSubtasks = model.HasSubtasks;
                modelToAdd.ID = model.ID;
                modelToAdd.Intervals = model.Intervals;
                modelToAdd.JobDescriptionID = model.JobDescriptionID; 
                
                //modelToAdd.Placement = model.Placement == "None" ? null : model.Placement;
                modelToAdd.StartDate = model.StartDate;
                modelToAdd.Title = model.Title;
                modelToAdd.ReplaceTaskID = model.ReplaceTaskID != null ? model.ReplaceTaskID : 0;

                if (model.byDate)
                {
                    modelToAdd.Day = DateTime.Parse(model.StartDate.ToString()).Day;
                    if (model.Frequency == "Yearly")
                    {
                        modelToAdd.Month = DateTime.Parse(model.StartDate.ToString()).ToString("MMMM");
                    }
                }
                else
                {
                    modelToAdd.Day = 0;
                    modelToAdd.Placement = model.Placement == "None" ? null : model.Placement;
                    modelToAdd.Month = model.Month == "None" ? null : model.Month;
                }


                db.JobTasks.Add(modelToAdd);                 
                db.SaveChanges();
                int newId = modelToAdd.ID;

                if (!model.byDate && (model.Frequency == "Monthly" || model.Frequency == "Yearly" || model.Frequency == "Weekly"))                    
                {                
                    foreach (var day in model.DaysOfWeek)
                    {
                        if (day.isChecked)
                        {
                            var weekDay = (from n in db.DaysOfWeeks.Where(x => x.ID.Equals(day.id))
                                           select n.Day).FirstOrDefault();

                            db.DaysOfWeek_Lookup.Add(new DaysOfWeek_Lookup
                            {
                                TaskID = newId,
                                DayOfWeek = weekDay,
                            });
                        }
                    }
                }

                
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: JobTasks/Edit/5
        public ActionResult Edit(int? id)
        { 
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var jobTask = db.JobTasks.Find(id);
            var checkedWeekDays = db.DaysOfWeek_Lookup.Where(x => x.TaskID == id).ToList();
            var model = new JobTaskViewModel();

            List<SelectListItem> MonthsListItems = new List<SelectListItem>();
            string[] months = { "None", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
            foreach (var item in months)
            {
                if (item == jobTask.Month)
                {
                    MonthsListItems.Add(new SelectListItem
                    {
                        Text = item,
                        Value = item,
                        Selected = true
                    });
                }
                else
                {
                    MonthsListItems.Add(new SelectListItem
                    {
                        Text = item,
                        Value = item
                    });
                }
            }

            if(jobTask.Day > 0)
            {
                model.byDate = true;
            }

            List<SelectListItem> PlaceListItems = new List<SelectListItem>();
            string[] placement = { "None", "First", "Second", "Third", "Fourth", "Last" };
            foreach (var item in placement)
            {
                if (item == jobTask.Placement)
                {
                    PlaceListItems.Add(new SelectListItem
                    {
                        Text = item,
                        Value = item,
                        Selected = true
                    });
                }
                else
                {
                    PlaceListItems.Add(new SelectListItem
                    {
                        Text = item,
                        Value = item
                    });
                }
            }

            model.ID = jobTask.ID;
            model.Title = jobTask.Title;
            model.Description = jobTask.Description;

            model.Placements = PlaceListItems;
            model.Months = MonthsListItems;

            List<SelectListItem> frequencyListItems = new List<SelectListItem>();
            string[] frecuencies = { "Daily", "Weekly", "Monthly", "Quarterly", "Half Year", "Yearly" };            
            foreach (var item in frecuencies)
            {
                if (item == jobTask.Frequency)
                {
                    frequencyListItems.Add(new SelectListItem
                    {
                        Text = item,
                        Value = item,
                        Selected = true
                    });
                }
                else
                {
                    frequencyListItems.Add(new SelectListItem
                    {
                        Text = item,
                        Value = item                        
                    });
                }
            }
            model.Frequencies = frequencyListItems;

            model.JobDescriptions = new List<SelectListItem>(
                                   from descriptions in db.JobDescriptions
                                   select new SelectListItem
                                   {
                                       Value = descriptions.ID.ToString(),
                                       Text = descriptions.Title,
                                       Selected = descriptions.ID == jobTask.JobDescriptionID ? true : false
                                   }).OrderBy(x => x.Text).ToList();

            model.ReplacementTasks = new List<SelectListItem>(
                                     from replaceTask in db.JobTasks
                                     select new SelectListItem
                                     {
                                         Value = replaceTask.ID.ToString(),
                                         Text = replaceTask.Title,
                                         Selected = replaceTask.ID == jobTask.ReplaceTaskID ? true : false
                                     });

            var checkBoxListItems = new List<CheckBoxList>();
            var days = db.DaysOfWeeks.ToList();
            foreach (var day in days)
            {
                bool containsDay = false;
                foreach (var checkedDay in checkedWeekDays)
                {
                    if (checkedDay.DayOfWeek == day.Day)
                    {
                        containsDay = true;
                        break;
                    }
                }
                if (containsDay)
                {
                    checkBoxListItems.Add(new CheckBoxList()
                    {
                        text = day.Day,
                        id = day.ID,
                        isChecked = true
                    });
                }
                else
                {
                    checkBoxListItems.Add(new CheckBoxList()
                    {
                        text = day.Day,
                        id = day.ID,
                        isChecked = false
                    });
                }
                
            }

            model.DaysOfWeek = checkBoxListItems;
            model.StartDate = jobTask.StartDate;
            model.EndDate = jobTask.EndDate;
            model.Intervals = jobTask.Intervals;
            model.HasSubtasks = jobTask.HasSubtasks != null ? (bool)jobTask.HasSubtasks : false;

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: JobTasks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Modified, ModifiedBy, Description, Frequency, HasSubtasks, ID, Intervals, JobDescriptionID, Title, ReplaceTaskID, DaysOfWeek, StartDate, EndDate, Placement, Month, byDate")] JobTaskViewModel model)
        {
            if (ModelState.IsValid)
            {                
                var modelToEdit = new JobTask();                
                modelToEdit.Modified = DateTime.Now;
                modelToEdit.ModifiedBy = User.Identity.Name.Split('\\')[1];                
                modelToEdit.Description = model.Description;                
                modelToEdit.Frequency = model.Frequency;
                modelToEdit.HasSubtasks = model.HasSubtasks;
                modelToEdit.ID = model.ID;
                modelToEdit.Intervals = model.Intervals;
                modelToEdit.JobDescriptionID = model.JobDescriptionID;                
                modelToEdit.Title = model.Title;
                modelToEdit.ReplaceTaskID = model.ReplaceTaskID != null ? model.ReplaceTaskID : 0;                
                modelToEdit.StartDate = model.StartDate;
                modelToEdit.EndDate = model.EndDate;

                if(model.byDate)
                {
                    modelToEdit.Day = DateTime.Parse(model.StartDate.ToString()).Day;

                    if (model.Frequency == "Yearly")
                    {
                        modelToEdit.Month = DateTime.Parse(model.StartDate.ToString()).ToString("MMMM");
                    }
                }
                else
                {
                    modelToEdit.Day = 0;
                    modelToEdit.Placement = model.Placement == "None" ? null : model.Placement;
                    modelToEdit.Month = model.Month == "None" ? null : model.Month;
                }

                db.JobTasks.Attach(modelToEdit);
                var entry = db.Entry(modelToEdit);
                entry.Property(e => e.Modified).IsModified = true;
                entry.Property(e => e.ModifiedBy).IsModified = true;
                entry.Property(e => e.Description).IsModified = true;
                entry.Property(e => e.Frequency).IsModified = true;
                entry.Property(e => e.HasSubtasks).IsModified = true;
                entry.Property(e => e.Intervals).IsModified = true;
                entry.Property(e => e.JobDescriptionID).IsModified = true;
                entry.Property(e => e.Title).IsModified = true;
                entry.Property(e => e.ReplaceTaskID).IsModified = true;
                entry.Property(e => e.Placement).IsModified = true;
                entry.Property(e => e.StartDate).IsModified = true;
                entry.Property(e => e.EndDate).IsModified = true;
                entry.Property(e => e.Day).IsModified = true;
                entry.Property(e => e.Month).IsModified = true;

                db.SaveChanges();

                var daysOfWeek_Lookup = db.DaysOfWeek_Lookup.Where(x => x.TaskID == model.ID);
                if (!model.byDate && (model.Frequency == "Monthly" || model.Frequency == "Yearly" || model.Frequency == "Weekly"))
                {
                    foreach (var day in daysOfWeek_Lookup)
                    {
                        db.DaysOfWeek_Lookup.Remove(day);
                    }
                    foreach (var day in model.DaysOfWeek)
                    {
                        if (day.isChecked)
                        {
                            var weekDay = (from n in db.DaysOfWeeks.Where(x => x.ID.Equals(day.id))
                                           select n.Day).FirstOrDefault();

                            db.DaysOfWeek_Lookup.Add(new DaysOfWeek_Lookup
                            {
                                TaskID = model.ID,
                                DayOfWeek = weekDay,
                            });
                        }
                    }
                }
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            return View(model);
        }

        // GET: JobTasks/Delete/5
        public ActionResult Delete(int? id)
        { 
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            JobTask jobTask = db.JobTasks.Find(id);
            if (jobTask == null)
            {
                return HttpNotFound();
            }
            return View(jobTask);
        }

        // POST: JobTasks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            JobTask jobTask = db.JobTasks.Find(id);
            var daysOfWeek_Lookup = db.DaysOfWeek_Lookup.Where(x => x.TaskID == id);
            var subTask_Lookup = db.Subtask_Lookup.Where(x => x.JobTaskID == id);
            var jobTask_Recurrence = db.JobTask_Recurrence.Where(x => x.TaskID == id);
            var subTask_Recurrence = db.Subtask_Recurrence.Where(x => x.ParentID == id);
            db.JobTasks.Remove(jobTask);
            foreach(var day in daysOfWeek_Lookup)
            {
                db.DaysOfWeek_Lookup.Remove(day);
            }
            foreach (var subTaskLookup in subTask_Lookup)
            {
                db.Subtask_Lookup.Remove(subTaskLookup);
            }
            foreach (var jobTaskRec in jobTask_Recurrence)
            {
                db.JobTask_Recurrence.Remove(jobTaskRec);
            }
            foreach (var subTaskRec in subTask_Recurrence)
            {
                db.Subtask_Recurrence.Remove(subTaskRec);
            }
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
