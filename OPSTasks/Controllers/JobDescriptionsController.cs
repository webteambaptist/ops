﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using OPSTasks.Models;
using OPSTasks.Models.JobDescriptionViewModels;
using OPSTasks.Helpers;

namespace OPSTasks.Controllers
{
    public class JobDescriptionsController : Controller
    {
        private Entities db = new Entities();

        // GET: JobDescriptions
        public ActionResult Index()
        {
            var userName = User.Identity.Name;
            var groups = UserSettings.GetGroups(userName);
            var userIsAdmin = groups.Where(x => x.Contains("OPS Admins"));
            var userIsUser = groups.Where(x => x.Contains("OPS Users"));

            if (userIsAdmin.Any())
            {
                ViewBag.IsAdmin = true;
                ViewBag.IsUser = false;
                return View(db.JobDescriptions.OrderBy(x => x.Title).ToList());
            }

            if (userIsUser.Any())
            {
                ViewBag.IsAdmin = false;
                ViewBag.IsUser = true;
                return View(db.JobDescriptions.OrderBy(x => x.Title).ToList());
            }

            return RedirectToAction("Index", "UnauthorizedAccess");            
        }

        // GET: JobDescriptions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            JobDescription jobDescription = db.JobDescriptions.Find(id);
            if (jobDescription == null)
            {
                return HttpNotFound();
            }
            return View(jobDescription);
        }

        public ActionResult UserDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            JobDescription jobDescription = db.JobDescriptions.Find(id);
            if (jobDescription == null)
            {
                return HttpNotFound();
            }
            return View(jobDescription);
        }

        // GET: JobDescriptions/Create
        public ActionResult Create()
        {
            var model = new JobDescriptionViewModel();

            model.Actions = new List<SelectListItem>(
                                   from actions in db.Actions
                                   select new SelectListItem
                                   {
                                       Value = actions.Action1,
                                       Text = actions.Action1
                                   });

            model.Categories = new List<SelectListItem>(
                                   from categories in db.Categories
                                   select new SelectListItem
                                   {
                                       Value = categories.Category1,
                                       Text = categories.Category1
                                   });

            model.FactorNames = new List<SelectListItem>(
                                   from factornames in db.FactorNames
                                   where factornames.Deleted == 0
                                   select new SelectListItem
                                   {
                                       Value = factornames.FactorName1,
                                       Text = factornames.FactorName1
                                   });
            return View(model);
        }

        // POST: JobDescriptions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Title,Description,Action,Category,AllowSkip,FactorName")] JobDescriptionViewModel model)
        {
            if (ModelState.IsValid)
            {
                var modelToAdd = new JobDescription();
                modelToAdd.Created = DateTime.Now;
                modelToAdd.CreatedBy = User.Identity.Name.Split('\\')[1];
                modelToAdd.Description = model.Description;               
                modelToAdd.ID = model.ID;                
                modelToAdd.Title = model.Title;
                modelToAdd.Action = model.Action;
                modelToAdd.Category = model.Category;
                modelToAdd.AllowSkip = model.AllowSkip;
                modelToAdd.FactorName = model.FactorName == null ? "N/A" : model.FactorName; 
                db.JobDescriptions.Add(modelToAdd);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: JobDescriptions/Edit/5
        public ActionResult Edit(int id)
        {
            JobDescription jobDescription = db.JobDescriptions.Find(id);
            var model = new JobDescriptionViewModel();

            model.Actions = new List<SelectListItem>(
                            from actions in db.Actions
                            select new SelectListItem
                            {
                                Value = actions.Action1,
                                Text = actions.Action1,
                                Selected = actions.Action1 == jobDescription.Action ? true : false
                            });

            model.Categories = new List<SelectListItem>(
                               from categories in db.Categories
                               select new SelectListItem
                               {
                                    Value = categories.Category1,
                                    Text = categories.Category1,
                                    Selected = categories.Category1 == jobDescription.Category ? true : false
                                });

            model.AllowSkip = (from ask in db.JobDescriptions.Where(x => x.ID == id) select ask.AllowSkip).FirstOrDefault();

            model.FactorNames = new List<SelectListItem>(
                                from factornames in db.FactorNames
                                where factornames.Deleted == 0
                                select new SelectListItem
                                {
                                    Value = factornames.FactorName1,
                                    Text = factornames.FactorName1,
                                    Selected = factornames.FactorName1 == jobDescription.FactorName ? true : false
                                });

            model.ID = id;
            model.Title = jobDescription.Title;
            model.Description = jobDescription.Description;

            if (jobDescription == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: JobDescriptions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]        
        [ValidateInput(false)]
        public ActionResult Edit([Bind(Include = "ID,Title,Description,Action,Category,AllowSkip,FactorName")] JobDescriptionViewModel model)
        {
            if (ModelState.IsValid)
            {
                var modelToEdit = new JobDescription();
                modelToEdit.Modified = DateTime.Now;
                modelToEdit.ModifiedBy = User.Identity.Name.Split('\\')[1];
                modelToEdit.Description = model.Description;
                modelToEdit.ID = model.ID;
                modelToEdit.Title = model.Title;
                modelToEdit.Action = model.Action;
                modelToEdit.Category = model.Category;
                modelToEdit.AllowSkip = model.AllowSkip;
                modelToEdit.FactorName = model.FactorName == null ? "N/A" : model.FactorName;
                db.JobDescriptions.Attach(modelToEdit);
                var entry = db.Entry(modelToEdit);
                entry.Property(e => e.Modified).IsModified = true;
                entry.Property(e => e.ModifiedBy).IsModified = true;
                entry.Property(e => e.Description).IsModified = true;
                entry.Property(e => e.Title).IsModified = true;
                entry.Property(e => e.Action).IsModified = true;
                entry.Property(e => e.Category).IsModified = true;
                entry.Property(e => e.AllowSkip).IsModified = true;
                entry.Property(e => e.FactorName).IsModified = true;                
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }

        // GET: JobDescriptions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            JobDescription jobDescription = db.JobDescriptions.Find(id);
            if (jobDescription == null)
            {
                return HttpNotFound();
            }
            return View(jobDescription);
        }

        // POST: JobDescriptions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            JobDescription jobDescription = db.JobDescriptions.Find(id);
            db.JobDescriptions.Remove(jobDescription);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
