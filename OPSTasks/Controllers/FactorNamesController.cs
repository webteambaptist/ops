﻿using OPSTasks.Helpers;
using OPSTasks.Models;
using OPSTasks.Models.FactorNamesViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace OPSTasks.Controllers
{
    public class FactorNamesController : Controller
    {
        private Entities db = new Entities();
        // GET: FactorNames
        public ActionResult Index()
        {
            var userName = User.Identity.Name;
            var groups = UserSettings.GetGroups(userName);
            var userIsAdmin = groups.Where(x => x.Contains("OPS Admins"));
            var userIsUser = groups.Where(x => x.Contains("OPS Users"));

            if (userIsAdmin.Any())
            {
                ViewBag.IsAdmin = true;
                ViewBag.IsUser = false;
                return View(db.FactorNames.OrderBy(x => x.FactorName1)
                    .Where(x=>x.Deleted==0).ToList());
            }
            if (userIsUser.Any())
            {
                ViewBag.IsAdmin = false;
                ViewBag.IsUser = true;
                return View(db.FactorNames.OrderBy(x => x.FactorName1)
                    .Where(x=>x.Deleted==0).ToList());
            }

            return RedirectToAction("Index", "UnauthorizedAccess");
        }
        public ActionResult Create()
        {
            var model = new FactorNamesViewModel();
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,FactorName")] FactorNamesViewModel model)
        {
            if (ModelState.IsValid)
            {
                var factor = new FactorName();
                factor.Created = DateTime.Now;
                factor.CreatedBy = User.Identity.Name.Split('\\')[1];
                factor.FactorName1 = model.FactorName;
                factor.ID = model.ID;

                db.FactorNames.Add(factor);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }
        public ActionResult Edit(int id)
        {
            FactorName factor = db.FactorNames.Find(id);
            var model = new FactorNamesViewModel();
            model.ID = id;
            model.FactorName = factor.FactorName1;

            if (factor == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include ="ID,FactorName")] FactorNamesViewModel model)
        {
            if (ModelState.IsValid)
            {
                var factor = new FactorName();
                factor.Created = model.Created;
                factor.CreatedBy = model.CreatedBy;
                factor.FactorName1 = model.FactorName;
                factor.ID = model.ID;
                factor.Updated = DateTime.Now;
                factor.UpdatedBy = User.Identity.Name.Split('\\')[1];
                db.FactorNames.Attach(factor);
                var entry = db.Entry(factor);
                entry.Property(e => e.Updated).IsModified = true;
                entry.Property(e => e.UpdatedBy).IsModified = true;
                entry.Property(e => e.FactorName1).IsModified = true;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FactorName factor = db.FactorNames.Find(id);
            if (factor==null)
            {
                return HttpNotFound();
            }
            return View(factor);
        }
        public ActionResult Delete (int? id)
        {
            if (id==null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FactorName factor = db.FactorNames.Find(id);
            if (factor==null)
            {
                return HttpNotFound();
            }
            return View(factor);
        }
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FactorName factor = db.FactorNames.Find(id);
            factor.Deleted = 1;
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }

}