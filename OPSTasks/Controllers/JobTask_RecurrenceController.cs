﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using OPSTasks.Models;
using System.DirectoryServices.AccountManagement;
using OPSTasks.Models.JobTask_RecurrenceViewModels;
using OPSTasks.Models.SubTask_RecurrenceViewModels;
using System.Data.Entity.Validation;
using OPSTasks.Helpers;

namespace OPSTasks.Controllers
{
    [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]

    public class JobTask_RecurrenceController : Controller
    {
        private Entities db = new Entities();

        // GET: JobTask_Recurrence
        public ActionResult Index(int? id, DateTime? startDate, DateTime? endDate, string sortOrder)
        {
            var userAuthenticated = User.Identity.IsAuthenticated;

            if (!userAuthenticated)
            {
                return RedirectToAction("Index", "UnauthorizedAccess");
            }

            var userName = User.Identity.Name;
            var groups = UserSettings.GetGroups(userName);
            var userIsAdmin = groups.Where(x => x.Contains("OPS Admins"));
            var userIsUser = groups.Where(x => x.Contains("OPS Users"));                               

            List<JobTask_RecurrenceViewModel> model = new List<JobTask_RecurrenceViewModel>();            

            if (id == null || (id != null && id == 1))
            {
                try
                {
                    model = (from jr in db.JobTask_Recurrence
                             from t in db.JobTasks.Where(x => x.ID == jr.TaskID).DefaultIfEmpty()
                             from jd in db.JobDescriptions.Where(x => x.ID == t.JobDescriptionID).DefaultIfEmpty()
                             where
                             jr.Completed == false
                             select new JobTask_RecurrenceViewModel
                             {
                                 Comments = jr.Comments != null && !String.IsNullOrEmpty(jr.Comments) ? jr.Comments.Substring(0, 50) + "..." : "",
                                 Completed = jr.Completed,
                                 Created = jr.Created,
                                 CreatedBy = jr.CreatedBy,
                                 DueDate = jr.DueDate,
                                 FactorName = jd.FactorName,
                                 FactorNameData = jr.FactorNameData != null && !String.IsNullOrEmpty(jr.FactorNameData) ? jr.FactorNameData.Substring(0, 50) + "..." : "",
                                 Finished = jr.Finished,
                                 FinishedBy = jr.FinishedBy,
                                 ID = jr.ID,
                                 IsActive = jr.IsActive,
                                 Modified = jr.Modified,
                                 ModifiedBy = jr.ModifiedBy,
                                 Started = jr.Started,
                                 StartedBy = jr.StartedBy,
                                 Task = t.Title,
                                 TaskID = jr.TaskID,
                                 JobDescriptionID = t.JobDescriptionID,
                                 JobDescriptionURL = jd.Title,
                                 Subtasks = (from jsr in db.Subtask_Recurrence
                                             from s in db.Subtasks.Where(x => x.ID == jsr.SubtaskID).DefaultIfEmpty()
                                             from sjd in db.JobDescriptions.Where(x => x.ID == s.JobDescriptionID).DefaultIfEmpty()
                                             from sjl in db.Subtask_Lookup.Where(x => x.JobTaskID == jr.TaskID && x.SubtaskID == jsr.SubtaskID).DefaultIfEmpty()
                                             where
                                             jsr.ParentID == jr.TaskID
                                             && jsr.DueDate == jr.DueDate
                                             && jsr.Completed == false
                                             select new Subtask_RecurrenceViewModel
                                             {
                                                 Comments = jsr.Comments != null && !String.IsNullOrEmpty(jsr.Comments) ? jsr.Comments.Substring(0, 50) + "..." : "",
                                                 Completed = jsr.Completed,
                                                 Created = jsr.Created,
                                                 CreatedBy = jsr.CreatedBy,
                                                 DueDate = jsr.DueDate,
                                                 FactorName = sjd.FactorName,
                                                 FactorNameData = jsr.FactorNameData != null && !String.IsNullOrEmpty(jsr.FactorNameData) ? jsr.FactorNameData.Substring(0, 50) + "..." : "",
                                                 Finished = jsr.Finished,
                                                 FinishedBy = jsr.FinishedBy,
                                                 ID = jsr.ID,
                                                 IsActive = jsr.IsActive,
                                                 Modified = jsr.Modified,
                                                 ModifiedBy = jsr.ModifiedBy,
                                                 Started = jsr.Started,
                                                 StartedBy = jsr.StartedBy,
                                                 Subtask = s.Title,
                                                 SubtaskID = jsr.SubtaskID,
                                                 ParentID = jsr.ParentID,
                                                 JobDescriptionID = s.JobDescriptionID,
                                                 JobDescriptionURL = sjd.Title,
                                                 OrderNumber = sjl.OrderNumber
                                             }).OrderBy(x => x.OrderNumber).ToList()
                             }).ToList();
                    //Original Order By Code.
                    //model = (from f in model.Where(x => x.DueDate < DateTime.Now.Date.AddDays(1)) select f).ToList();
                    //model = (from f in model select f).OrderByDescending(x => x.DueDate.Value.Date).ThenBy(x => x.Task).ToList();

                    foreach (var item in model)
                    {
                        var value = item.Task.Split('-');   
                        if (value.Length >= 5)
                        {
                            item.Title1 = value[0].ToString();
                            item.Title2 = value[1].ToString();
                            item.Title3 = value[2].ToString();
                            item.Title4 = value[3].ToString() + " " + value[4].ToString();
                        }
                        else if (value.Length < 5)
                        {
                            item.Title1 = value[0].ToString();
                            item.Title2 = value[1].ToString();
                            item.Title3 = value[2].ToString();
                            item.Title4 = value[3].ToString();
                        }
                
                        foreach (var subitem in item.Subtasks)
                        {
                            var value2 = subitem.Subtask.Split('-');
                            subitem.Title1 = value2[0].ToString();
                            subitem.Title2 = value2[1].ToString();
                            subitem.Title3 = value2[2].ToString();
                            subitem.Title4 = value2[3].ToString();
                        }
                    }
                    
                    ViewBag.DateSortParm = sortOrder == "DueDate" ? "date_desc" : "DueDate";
                    ViewBag.TaskSortParm = sortOrder == "Task" ? "task_desc" : "Task";

                    switch (sortOrder)
                    {
                        case "DueDate":
                            model = (from f in model.Where(x => x.DueDate < DateTime.Now.Date.AddDays(1)) select f).ToList();
                            model = (from f in model select f).OrderBy(x => x.DueDate).ToList();
                            break;
                        case "date_desc":
                            model = (from f in model.Where(x => x.DueDate < DateTime.Now.Date.AddDays(1)) select f).ToList();
                            model = (from f in model select f).OrderByDescending(x => x.DueDate).ToList();
                            break;
                        case "Task":
                            model = (from f in model select f).OrderBy(x => x.Task).ToList();
                            break;
                        case "task_desc":
                            model = (from f in model select f).OrderByDescending(x => x.Task).ToList();
                            break;
                        default:
                            //model = (from f in model.Where(x => x.DueDate < DateTime.Now.Date.AddDays(1)) select f).ToList();
                            //model = (from f in model select f).OrderByDescending(x => x.DueDate.Value.Date < DateTime.Now.Date).ThenBy(x => x.DueDate.Value.Date >= DateTime.Now.Date).ToList();


                            model = (from f in model.Where(x => x.DueDate < DateTime.Now.Date.AddDays(1)) select f).ToList();
                            model = (from f in model select f).OrderBy(x => x.DueDate.Value.Date).ThenBy(x => x.Task).ToList();


                            //model = (from f in model select f).OrderBy(x => x.DueDate >= DateTime.Now).ThenBy(x => x.DueDate < DateTime.Now).ThenBy(x => x.Task).ToList();
                            //model = (from f in model select f).OrderBy(x => x.DueDate.Value.Date).ThenBy(x => x.Task).ToList();


                            //model = (from f in model select f).OrderByDescending(x => x.DueDate < DateTime.Now).ToList();
                            //model = (from f in model select f).OrderBy(x => x.DueDate > DateTime.Now).ThenByDescending(x => x.DueDate < DateTime.Now).ToList();

                            //model = (from f in model select f).OrderBy(x => x.Started).ThenByDescending(x => x.DueDate.Value.Date).ThenBy(x => x.Task).ToList();
                            //Original Code
                            //model = (from f in model.Where(x => x.DueDate < DateTime.Now.Date.AddDays(1)) select f).ToList();
                            //model = (from f in model select f).OrderByDescending(x => x.DueDate.Value.Date).ThenBy(x => x.Task).ToList();
                            //model = (from f in model select f).OrderByDescending(x => x.DueDate.Value.Date).ThenBy(x => x.Task).ToList();

                            //model = (from f in model.Where(x => x.DueDate < DateTime.Now) select f).ToList();
                            //        model = (from f in model select f).OrderByDescending(x => x.DueDate.Value.Date).ThenBy(x => x.Task).ToList();
                            break;
                    }
                    ViewBag.View = "Current Tasks";

                }
                catch (Exception ex)
                {

                }
            }

            if (id != null && id == 2)
            {
                try
                {
                    model = (from jr in db.JobTask_Recurrence
                             from t in db.JobTasks.Where(x => x.ID == jr.TaskID).DefaultIfEmpty()
                             from jd in db.JobDescriptions.Where(x => x.ID == t.JobDescriptionID).DefaultIfEmpty()
                             select new JobTask_RecurrenceViewModel
                             {
                                 Comments = jr.Comments != null && !String.IsNullOrEmpty(jr.Comments) ? jr.Comments.Substring(0, 50) + "..." : "",
                                 Completed = jr.Completed,
                                 Created = jr.Created,
                                 CreatedBy = jr.CreatedBy,
                                 DueDate = jr.DueDate,
                                 FactorName = jd.FactorName,
                                 FactorNameData = jr.FactorNameData != null && !String.IsNullOrEmpty(jr.FactorNameData) ? jr.FactorNameData.Substring(0, 50) + "..." : "",
                                 Finished = jr.Finished,
                                 FinishedBy = jr.FinishedBy,
                                 ID = jr.ID,
                                 IsActive = jr.IsActive,
                                 Modified = jr.Modified,
                                 ModifiedBy = jr.ModifiedBy,
                                 Started = jr.Started,
                                 StartedBy = jr.StartedBy,
                                 Task = t.Title,
                                 TaskID = jr.TaskID,
                                 JobDescriptionID = t.JobDescriptionID,
                                 JobDescriptionURL = jd.Title,
                                 Subtasks = (from jsr in db.Subtask_Recurrence
                                             from s in db.Subtasks.Where(x => x.ID == jsr.SubtaskID).DefaultIfEmpty()
                                             from sjd in db.JobDescriptions.Where(x => x.ID == s.JobDescriptionID).DefaultIfEmpty()
                                             from sjl in db.Subtask_Lookup.Where(x => x.JobTaskID == jr.TaskID && x.SubtaskID == jsr.SubtaskID).DefaultIfEmpty()
                                             where
                                             jsr.ParentID == jr.TaskID
                                             && jsr.DueDate == jr.DueDate
                                             select new Subtask_RecurrenceViewModel
                                             {
                                                 Comments = jsr.Comments != null && !String.IsNullOrEmpty(jsr.Comments) ? jsr.Comments.Substring(0, 50) + "..." : "",
                                                 Completed = jsr.Completed,
                                                 Created = jsr.Created,
                                                 CreatedBy = jsr.CreatedBy,
                                                 DueDate = jsr.DueDate,
                                                 FactorName = sjd.FactorName,
                                                 FactorNameData = jsr.FactorNameData != null && !String.IsNullOrEmpty(jsr.FactorNameData) ? jsr.FactorNameData.Substring(0, 50) + "..." : "",
                                                 Finished = jsr.Finished,
                                                 FinishedBy = jsr.FinishedBy,
                                                 ID = jsr.ID,
                                                 IsActive = jsr.IsActive,
                                                 Modified = jsr.Modified,
                                                 ModifiedBy = jsr.ModifiedBy,
                                                 Started = jsr.Started,
                                                 StartedBy = jsr.StartedBy,
                                                 Subtask = s.Title,
                                                 SubtaskID = jsr.SubtaskID,
                                                 ParentID = jsr.ParentID,
                                                 JobDescriptionID = s.JobDescriptionID,
                                                 JobDescriptionURL = sjd.Title,
                                                 OrderNumber = sjl.OrderNumber
                                             }).OrderBy(x => x.OrderNumber.Value).ToList()
                             }).ToList();

                    model = (from f in model.Where(x => x.DueDate >= DateTime.Today.Date) select f).ToList();
                    model = (from f in model.Where(x => x.DueDate < DateTime.Today.Date.AddDays(7)) select f).ToList();
                    model = (from f in model select f).OrderByDescending(x => x.DueDate.Value.Date).ThenBy(x => x.Task).ToList();

                    foreach (var item in model)
                    {
                        var value = item.Task.Split('-');
                        if (value.Length == 5)
                        {
                            item.Title1 = value[0].ToString();
                            item.Title2 = value[1].ToString();
                            item.Title3 = value[2].ToString();
                            item.Title4 = value[3].ToString() + " " + value[4].ToString();
                        }
                        else if (value.Length < 5)
                        {
                            item.Title1 = value[0].ToString();
                            item.Title2 = value[1].ToString();
                            item.Title3 = value[2].ToString();
                            item.Title4 = value[3].ToString();
                        }

                        foreach (var subitem in item.Subtasks)
                        {
                            var value2 = subitem.Subtask.Split('-');
                            subitem.Title1 = value2[0].ToString();
                            subitem.Title2 = value2[1].ToString();
                            subitem.Title3 = value2[2].ToString();
                            subitem.Title4 = value2[3].ToString();
                        }
                    }

                    ViewBag.View = "Current Week";
                }
                catch (Exception ex)
                {

                }
            }

            #region Overdue option
            //if (id != null && id == 3)
            //{
            //    try
            //    {
            //        model = (from jr in db.JobTask_Recurrence
            //                 from t in db.JobTasks.Where(x => x.ID == jr.TaskID).DefaultIfEmpty()
            //                 from jd in db.JobDescriptions.Where(x => x.ID == t.JobDescriptionID).DefaultIfEmpty()
            //                 where
            //                 jr.Completed == false
            //                 select new JobTask_RecurrenceViewModel
            //                 {
            //                     Comments = jr.Comments != null && !String.IsNullOrEmpty(jr.Comments) ? jr.Comments.Substring(0, 50) + "..." : "",
            //                     Completed = jr.Completed,
            //                     Created = jr.Created,
            //                     CreatedBy = jr.CreatedBy,
            //                     DueDate = jr.DueDate,
            //                     FactorName = jd.FactorName,
            //                     FactorNameData = jr.FactorNameData != null && !String.IsNullOrEmpty(jr.FactorNameData) ? jr.FactorNameData.Substring(0, 50) + "..." : "",
            //                     Finished = jr.Finished,
            //                     FinishedBy = jr.FinishedBy,
            //                     ID = jr.ID,
            //                     IsActive = jr.IsActive,
            //                     Modified = jr.Modified,
            //                     ModifiedBy = jr.ModifiedBy,
            //                     Started = jr.Started,
            //                     StartedBy = jr.StartedBy,
            //                     Task = t.Title,
            //                     TaskID = jr.TaskID,
            //                     JobDescriptionID = t.JobDescriptionID,
            //                     JobDescriptionURL = jd.Title,
            //                     Subtasks = (from jsr in db.Subtask_Recurrence
            //                                 from s in db.Subtasks.Where(x => x.ID == jsr.SubtaskID).DefaultIfEmpty()
            //                                 from sjd in db.JobDescriptions.Where(x => x.ID == s.JobDescriptionID).DefaultIfEmpty()
            //                                 from sjl in db.Subtask_Lookup.Where(x => x.JobTaskID == jr.TaskID && x.SubtaskID == jsr.SubtaskID).DefaultIfEmpty()
            //                                 where
            //                                 jsr.ParentID == jr.TaskID
            //                                 && jsr.DueDate == jr.DueDate
            //                                 && jsr.Completed == false
            //                                 select new Subtask_RecurrenceViewModel
            //                                 {
            //                                     Comments = jsr.Comments != null && !String.IsNullOrEmpty(jsr.Comments) ? jsr.Comments.Substring(0, 50) + "..." : "",
            //                                     Completed = jsr.Completed,
            //                                     Created = jsr.Created,
            //                                     CreatedBy = jsr.CreatedBy,
            //                                     DueDate = jsr.DueDate,
            //                                     FactorName = sjd.FactorName,
            //                                     FactorNameData = jsr.FactorNameData != null && !String.IsNullOrEmpty(jsr.FactorNameData) ? jsr.FactorNameData.Substring(0, 50) + "..." : "",
            //                                     Finished = jsr.Finished,
            //                                     FinishedBy = jsr.FinishedBy,
            //                                     ID = jsr.ID,
            //                                     IsActive = jsr.IsActive,
            //                                     Modified = jsr.Modified,
            //                                     ModifiedBy = jsr.ModifiedBy,
            //                                     Started = jsr.Started,
            //                                     StartedBy = jsr.StartedBy,
            //                                     Subtask = s.Title,
            //                                     SubtaskID = jsr.SubtaskID,
            //                                     ParentID = jsr.ParentID,
            //                                     JobDescriptionID = s.JobDescriptionID,
            //                                     JobDescriptionURL = sjd.Title,
            //                                     OrderNumber = sjl.OrderNumber
            //                                 }).OrderBy(x => x.OrderNumber.Value).ToList()
            //                 }).ToList();

            //        model = (from f in model.Where(x => x.DueDate < DateTime.Now) select f).ToList();
            //        model = (from f in model select f).OrderByDescending(x => x.DueDate.Value.Date).ThenBy(x => x.Task).ToList();

            //        foreach (var item in model)
            //        {
            //            var value = item.Task.Split('-');
            //            if (value.Length == 5)
            //            {
            //                item.Title1 = value[0].ToString();
            //                item.Title2 = value[1].ToString();
            //                item.Title3 = value[2].ToString();
            //                item.Title4 = value[3].ToString() + " " + value[4].ToString();
            //            }
            //            else if (value.Length < 5)
            //            {
            //                item.Title1 = value[0].ToString();
            //                item.Title2 = value[1].ToString();
            //                item.Title3 = value[2].ToString();
            //                item.Title4 = value[3].ToString();
            //            }

            //            foreach (var subitem in item.Subtasks)
            //            {
            //                var value2 = subitem.Subtask.Split('-');
            //                subitem.Title1 = value2[0].ToString();
            //                subitem.Title2 = value2[1].ToString();
            //                subitem.Title3 = value2[2].ToString();
            //                subitem.Title4 = value2[3].ToString();
            //            }
            //        }

            //        ViewBag.View = "Overdue";

            //    }
            //    catch (Exception ex)
            //    {

            //    }
            //}
            #endregion

            if (id != null && id == 4)
            {
                /** This is for initial load and if no dates are used for search **/
                if (startDate==null&&endDate==null)
                {
                    try
                    {
                        model = (from jr in db.JobTask_Recurrence
                             from t in db.JobTasks.Where(x => x.ID == jr.TaskID).DefaultIfEmpty()
                             from jd in db.JobDescriptions.Where(x => x.ID == t.JobDescriptionID).DefaultIfEmpty()
                             select new JobTask_RecurrenceViewModel
                             {
                                 Comments = jr.Comments != null && !String.IsNullOrEmpty(jr.Comments) ? jr.Comments.Substring(0, 50) + "..." : "",
                                 Completed = jr.Completed,
                                 Created = jr.Created,
                                 CreatedBy = jr.CreatedBy,
                                 DueDate = jr.DueDate,
                                 FactorName = jd.FactorName,
                                 FactorNameData = jr.FactorNameData != null && !String.IsNullOrEmpty(jr.FactorNameData) ? jr.FactorNameData.Substring(0, 50) + "..." : "",
                                 Finished = jr.Finished,
                                 FinishedBy = jr.FinishedBy,
                                 ID = jr.ID,
                                 IsActive = jr.IsActive,
                                 Modified = jr.Modified,
                                 ModifiedBy = jr.ModifiedBy,
                                 Started = jr.Started,
                                 StartedBy = jr.StartedBy,
                                 Task = t.Title,
                                 TaskID = jr.TaskID,
                                 JobDescriptionID = t.JobDescriptionID,
                                 JobDescriptionURL = jd.Title,
                                 Subtasks = (from jsr in db.Subtask_Recurrence
                                             from s in db.Subtasks.Where(x => x.ID == jsr.SubtaskID).DefaultIfEmpty()
                                             from sjd in db.JobDescriptions.Where(x => x.ID == s.JobDescriptionID).DefaultIfEmpty()
                                             from sjl in db.Subtask_Lookup.Where(x => x.JobTaskID == jr.TaskID && x.SubtaskID == jsr.SubtaskID).DefaultIfEmpty()
                                             where
                                             jsr.ParentID == jr.TaskID
                                             && jsr.DueDate == jr.DueDate
                                             select new Subtask_RecurrenceViewModel
                                             {
                                                 Comments = jsr.Comments != null && !String.IsNullOrEmpty(jsr.Comments) ? jsr.Comments.Substring(0, 50) + "..." : "",
                                                 Completed = jsr.Completed,
                                                 Created = jsr.Created,
                                                 CreatedBy = jsr.CreatedBy,
                                                 DueDate = jsr.DueDate,
                                                 FactorName = sjd.FactorName,
                                                 FactorNameData = jsr.FactorNameData != null && !String.IsNullOrEmpty(jsr.FactorNameData) ? jsr.FactorNameData.Substring(0, 50) + "..." : "",
                                                 Finished = jsr.Finished,
                                                 FinishedBy = jsr.FinishedBy,
                                                 ID = jsr.ID,
                                                 IsActive = jsr.IsActive,
                                                 Modified = jsr.Modified,
                                                 ModifiedBy = jsr.ModifiedBy,
                                                 Started = jsr.Started,
                                                 StartedBy = jsr.StartedBy,
                                                 Subtask = s.Title,
                                                 SubtaskID = jsr.SubtaskID,
                                                 ParentID = jsr.ParentID,
                                                 JobDescriptionID = s.JobDescriptionID,
                                                 JobDescriptionURL = sjd.Title,
                                                 OrderNumber = sjl.OrderNumber
                                             }).OrderBy(x => x.OrderNumber.Value).ToList()
                             }).ToList();

                        DateTime today = DateTime.Today;
                        DateTime pastSixdays = today.AddDays(-6);

                        //model = (from f in model.Where(x => x.DueDate.Value.Date >= pastSixdays && x.DueDate.Value.Date <= DateTime.Now.Date) select f).OrderByDescending(x => x.DueDate).ThenBy(x => x.Task).ToList();                        

                        model = (from f in model.Where(x => x.DueDate.Value.Date >= pastSixdays && x.DueDate.Value.Date <= DateTime.Now.Date) select f).ToList();
                        model = (from f in model select f).OrderByDescending(x => x.DueDate.Value.Date).ThenBy(x => x.Task).ToList();

                        //model = (from f in model.Where(x => x.DueDate > DateTime.Now.AddDays(-7)) select f).OrderBy(x => x.DueDate).ToList();
                        ////model = (from f in model.Where(x => x.Finished > DateTime.Now.Date.AddDays(-7)) select f).OrderBy(x => x.Finished).ToList();
                        //model = (from f in model select f).OrderByDescending(x => x.DueDate.Value.Date.AddDays(-7)).ThenBy(x => x.Task).ToList();

                        foreach (var item in model)
                        {
                            var value = item.Task.Split('-');
                            if (value.Length == 5)
                            {
                                item.Title1 = value[0].ToString();
                                item.Title2 = value[1].ToString();
                                item.Title3 = value[2].ToString();
                                item.Title4 = value[3].ToString() + " " + value[4].ToString();
                            }
                            else if (value.Length < 5)
                            {
                                item.Title1 = value[0].ToString();
                                item.Title2 = value[1].ToString();
                                item.Title3 = value[2].ToString();
                                item.Title4 = value[3].ToString();
                            }

                            foreach (var subitem in item.Subtasks)
                            {
                                var value2 = subitem.Subtask.Split('-');
                                subitem.Title1 = value2[0].ToString();
                                subitem.Title2 = value2[1].ToString();
                                subitem.Title3 = value2[2].ToString();
                                subitem.Title4 = value2[3].ToString();
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
                /** This is for searching by date fields **/
                else
                {
                    try
                    {
                        model = (from jr in db.JobTask_Recurrence
                                 from t in db.JobTasks.Where(x => x.ID == jr.TaskID).DefaultIfEmpty()
                                 from jd in db.JobDescriptions.Where(x => x.ID == t.JobDescriptionID).DefaultIfEmpty()
                                 select new JobTask_RecurrenceViewModel
                                 {
                                     Comments = jr.Comments != null && !String.IsNullOrEmpty(jr.Comments) ? jr.Comments.Substring(0, 50) + "..." : "",
                                     Completed = jr.Completed,
                                     Created = jr.Created,
                                     CreatedBy = jr.CreatedBy,
                                     DueDate = jr.DueDate,
                                     FactorName = jd.FactorName,
                                     FactorNameData = jr.FactorNameData != null && !String.IsNullOrEmpty(jr.FactorNameData) ? jr.FactorNameData.Substring(0, 50) + "..." : "",
                                     Finished = jr.Finished,
                                     FinishedBy = jr.FinishedBy,
                                     ID = jr.ID,
                                     IsActive = jr.IsActive,
                                     Modified = jr.Modified,
                                     ModifiedBy = jr.ModifiedBy,
                                     Started = jr.Started,
                                     StartedBy = jr.StartedBy,
                                     Task = t.Title,
                                     TaskID = jr.TaskID,
                                     JobDescriptionID = t.JobDescriptionID,
                                     JobDescriptionURL = jd.Title,
                                     Subtasks = (from jsr in db.Subtask_Recurrence
                                                 from s in db.Subtasks.Where(x => x.ID == jsr.SubtaskID).DefaultIfEmpty()
                                                 from sjd in db.JobDescriptions.Where(x => x.ID == s.JobDescriptionID).DefaultIfEmpty()
                                                 from sjl in db.Subtask_Lookup.Where(x => x.JobTaskID == jr.TaskID && x.SubtaskID == jsr.SubtaskID).DefaultIfEmpty()
                                                 where
                                                 jsr.ParentID == jr.TaskID
                                                 && jsr.DueDate == jr.DueDate
                                                 select new Subtask_RecurrenceViewModel
                                                 {
                                                     Comments = jsr.Comments != null && !String.IsNullOrEmpty(jsr.Comments) ? jsr.Comments.Substring(0, 50) + "..." : "",
                                                     Completed = jsr.Completed,
                                                     Created = jsr.Created,
                                                     CreatedBy = jsr.CreatedBy,
                                                     DueDate = jsr.DueDate,
                                                     FactorName = sjd.FactorName,
                                                     FactorNameData = jsr.FactorNameData != null && !String.IsNullOrEmpty(jsr.FactorNameData) ? jsr.FactorNameData.Substring(0, 50) + "..." : "",
                                                     Finished = jsr.Finished,
                                                     FinishedBy = jsr.FinishedBy,
                                                     ID = jsr.ID,
                                                     IsActive = jsr.IsActive,
                                                     Modified = jsr.Modified,
                                                     ModifiedBy = jsr.ModifiedBy,
                                                     Started = jsr.Started,
                                                     StartedBy = jsr.StartedBy,
                                                     Subtask = s.Title,
                                                     SubtaskID = jsr.SubtaskID,
                                                     ParentID = jsr.ParentID,
                                                     JobDescriptionID = s.JobDescriptionID,
                                                     JobDescriptionURL = sjd.Title,
                                                     OrderNumber = sjl.OrderNumber
                                                 }).OrderBy(x => x.OrderNumber.Value).ToList()
                                 }).ToList();
                        //model = (from f in model.Where(x => x.DueDate > DateTime.Now.Date.AddDays(-7)) select f).OrderBy(x => x.DueDate).ToList();
                        if (startDate == null && endDate != null)
                        {
                            model = (from f in model.Where(x => x.DueDate.Value.Date >= endDate.Value.AddDays(-7) && x.DueDate.Value.Date <= endDate) select f).ToList();
                        }
                        if (startDate != null && endDate == null)
                        {
                            model = (from f in model.Where(x => x.DueDate.Value.Date >= startDate && x.DueDate.Value.Date <= startDate.Value.AddDays(+7)) select f).ToList();
                        }
                        else
                        {
                            model = (from f in model.Where(x => x.DueDate.Value.Date >= startDate && x.DueDate.Value.Date <= endDate) select f).ToList();
                        }
                        model = (from f in model select f).OrderByDescending(x => x.DueDate.Value.Date).ThenBy(x => x.Task).ToList();

                        foreach (var item in model)
                        {
                            var value = item.Task.Split('-');
                            if (value.Length == 5)
                            {
                                item.Title1 = value[0].ToString();
                                item.Title2 = value[1].ToString();
                                item.Title3 = value[2].ToString();
                                item.Title4 = value[3].ToString() + " " + value[4].ToString();
                            }
                            else if (value.Length < 5)
                            {
                                item.Title1 = value[0].ToString();
                                item.Title2 = value[1].ToString();
                                item.Title3 = value[2].ToString();
                                item.Title4 = value[3].ToString();
                            }

                            foreach (var subitem in item.Subtasks)
                            {
                                var value2 = subitem.Subtask.Split('-');
                                subitem.Title1 = value2[0].ToString();
                                subitem.Title2 = value2[1].ToString();
                                subitem.Title3 = value2[2].ToString();
                                subitem.Title4 = value2[3].ToString();
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }

                ViewBag.View = "Historical";
            }

            if (userIsAdmin.Any())
            {
                ViewBag.IsAdmin = true;
                ViewBag.IsUser = false;
                return View(model);
            }

            if (userIsUser.Any())
            {
                ViewBag.IsAdmin = false;
                ViewBag.IsUser = true;  
                return View(model);
            }

            return RedirectToAction("Index", "UnauthorizedAccess");
        }

        // GET: JobTask_Recurrence/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            JobTask_Recurrence jobTask_Recurrence = db.JobTask_Recurrence.Find(id);
            if (jobTask_Recurrence == null)
            {
                return HttpNotFound();
            }
            return View(jobTask_Recurrence);
        }

        // GET: JobTask_Recurrence/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: JobTask_Recurrence/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,TaskID,ReplaceTaskID,DueDate,FactorNameData,Started,StartedBy,Finished,FinishedBy,Completed,Comments,IsActive,Created,Frequency,Intervals")] JobTask_Recurrence jobTask_Recurrence)
        {
            if (ModelState.IsValid)
            {
                db.JobTask_Recurrence.Add(jobTask_Recurrence);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(jobTask_Recurrence);
        }

        // GET: JobTask_Recurrence/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            JobTask_Recurrence jobTask_Recurrence = db.JobTask_Recurrence.Find(id);
            if (jobTask_Recurrence == null)
            {
                return HttpNotFound();
            }
            return View(jobTask_Recurrence);
        }

        // POST: JobTask_Recurrence/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,TaskID,ReplaceTaskID,DueDate,FactorNameData,Started,StartedBy,Finished,FinishedBy,Completed,Comments,IsActive,Created,Frequency,Intervals")] JobTask_Recurrence jobTask_Recurrence)
        {
            if (ModelState.IsValid)
            {
                db.Entry(jobTask_Recurrence).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(jobTask_Recurrence);
        }

        // GET: JobTask_Recurrence/EditTaskFactorNameData/5
        public ActionResult EditTaskFactorNameData(int? id)
        {
            //var adminCookie = Request.Cookies["OPSUserAdmin"];
            //if (adminCookie != null)
            //{
            //    if (adminCookie.Value == "true")
            //    {
            //        ViewBag.IsAdmin = true;
            //    }
            //    else
            //    {
            //        ViewBag.IsAdmin = false;
            //    }
            //}
            //else
            //{
            //    return RedirectToAction("Index", "JobTask_Recurrence");
            //}

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            JobTask_Recurrence jobTask_Recurrence = db.JobTask_Recurrence.Find(id);
            var model = new JobTask_RecurrenceViewModel
            {
                FactorNameData = jobTask_Recurrence.FactorNameData
            };

            if (jobTask_Recurrence == null)
            {
                return HttpNotFound();
            }

            return View(model);
        }

        // POST: JobTask_Recurrence/EditTaskFactorNameData/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTaskFactorNameData([Bind(Include = "ID,FactorNameData,AddFactorNameData")] JobTask_RecurrenceViewModel model)
        {
            if (ModelState.IsValid)
            {
                var modelToEdit = new JobTask_Recurrence
                {
                    ID = model.ID,
                    FactorNameData = String.IsNullOrEmpty(model.FactorNameData)
                    ? "(" + DateTime.Now.ToString() + " " + User.Identity.Name.Split('\\')[1] + ") " + model.AddFactorNameData
                    : model.FactorNameData + " (" + DateTime.Now.ToString() + " " + User.Identity.Name.Split('\\')[1] + ") " + model.AddFactorNameData
                };

                db.JobTask_Recurrence.Attach(modelToEdit);
                var entry = db.Entry(modelToEdit);
                entry.Property(e => e.FactorNameData).IsModified = true;                
                db.SaveChanges();                
                return RedirectToAction("EditTaskFactorNameData", model.ID);
            }
            return View(EditTaskComments(model.ID));
        }

        // GET: JobTask_Recurrence/EditSubtaskFactorNameData/5
        public ActionResult EditSubtaskFactorNameData(int? id)
        {
            //var adminCookie = Request.Cookies["OPSUserAdmin"];
            //if (adminCookie != null)
            //{
            //    if (adminCookie.Value == "true")
            //    {
            //        ViewBag.IsAdmin = true;
            //    }
            //    else
            //    {
            //        ViewBag.IsAdmin = false;
            //    }
            //}
            //else
            //{
            //    return RedirectToAction("Index", "JobTask_Recurrence");
            //}

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Subtask_Recurrence subtask_Recurrence = db.Subtask_Recurrence.Find(id);
            var model = new Subtask_RecurrenceViewModel
            {
                FactorNameData = subtask_Recurrence.FactorNameData
            };

            if (subtask_Recurrence == null)
            {
                return HttpNotFound();
            }

            return View(model);
        }

        // POST: JobTask_Recurrence/EditSubtaskFactorNameData/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditSubtaskFactorNameData([Bind(Include = "ID,FactorNameData,AddFactorNameData")] Subtask_RecurrenceViewModel model)
        {
            if (ModelState.IsValid)
            {
                var modelToEdit = new Subtask_Recurrence
                {
                    ID = model.ID != null ? (int)model.ID : 0,
                    FactorNameData = String.IsNullOrEmpty(model.FactorNameData)
                    ? "(" + DateTime.Now.ToString() + " " + User.Identity.Name.Split('\\')[1] + ") " + model.AddFactorNameData
                    : model.FactorNameData + " (" + DateTime.Now.ToString() + " " + User.Identity.Name.Split('\\')[1] + ") " + model.AddFactorNameData
                };

                db.Subtask_Recurrence.Attach(modelToEdit);
                var entry = db.Entry(modelToEdit);
                entry.Property(e => e.FactorNameData).IsModified = true;                
                db.SaveChanges();                
                return RedirectToAction("EditSubtaskFactorNameData", model.ID);
            }

            return View(EditTaskComments(model.ID));
        }

        // GET: JobTask_Recurrence/EditTaskComments/5
        public ActionResult EditTaskComments(int? id)
        {
            //var adminCookie = Request.Cookies["OPSUserAdmin"];
            //if (adminCookie != null)
            //{
            //    if (adminCookie.Value == "true")
            //    {
            //        ViewBag.IsAdmin = true;
            //    }
            //    else
            //    {
            //        ViewBag.IsAdmin = false;
            //    }
            //}
            //else
            //{
            //    return RedirectToAction("Index", "JobTask_Recurrence");
            //}

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            JobTask_Recurrence jobTask_Recurrence = db.JobTask_Recurrence.Find(id);
            var model = new JobTask_RecurrenceViewModel
            {
                Comments = jobTask_Recurrence.Comments
            };

            if (jobTask_Recurrence == null)
            {
                return HttpNotFound();
            }

            return View(model);
        }

        // POST: JobTask_Recurrence/EditTaskComments/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTaskComments([Bind(Include = "ID,Comments,AddComment")] JobTask_RecurrenceViewModel model)
        {           
            if (ModelState.IsValid)
            {
                var modelToEdit = new JobTask_Recurrence
                {
                    ID = model.ID,
                    Comments = String.IsNullOrEmpty(model.Comments) 
                    ? "(" + DateTime.Now.ToString() + " " + User.Identity.Name.Split('\\')[1] + ") " + model.AddComment
                    : model.Comments + " (" + DateTime.Now.ToString() + " " + User.Identity.Name.Split('\\')[1] + ") " + model.AddComment
                };
                db.JobTask_Recurrence.Attach(modelToEdit);
                var entry = db.Entry(modelToEdit);
                entry.Property(e => e.Comments).IsModified = true;                
                db.SaveChanges();
                return RedirectToAction("EditTaskComments", model.ID);
            }
            return View(EditTaskComments(model.ID));
        }

        // GET: JobTask_Recurrence/EditSubtaskComments/5
        public ActionResult EditSubtaskComments(int? id)
        {
            //var adminCookie = Request.Cookies["OPSUserAdmin"];
            //if (adminCookie != null)
            //{
            //    if (adminCookie.Value == "true")
            //    {
            //        ViewBag.IsAdmin = true;
            //    }
            //    else
            //    {
            //        ViewBag.IsAdmin = false;
            //    }
            //}
            //else
            //{
            //    return RedirectToAction("Index", "JobTask_Recurrence");
            //}

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Subtask_Recurrence subtask_Recurrence = db.Subtask_Recurrence.Find(id);
            var model = new Subtask_RecurrenceViewModel
            {
                Comments = subtask_Recurrence.Comments
            };

            if (subtask_Recurrence == null)
            {
                return HttpNotFound();
            }

            return View(model);
        }

        // POST: JobTask_Recurrence/EditSubtaskComments/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditSubtaskComments([Bind(Include = "ID,Comments,AddComment")] Subtask_RecurrenceViewModel model)
        {
            if (ModelState.IsValid)
            {
                var modelToEdit = new Subtask_Recurrence
                {
                    ID = model.ID != null ? (int)model.ID : 0,
                    Comments = String.IsNullOrEmpty(model.Comments)
                    ? "(" + DateTime.Now.ToString() + " " + User.Identity.Name.Split('\\')[1] + ") " + model.AddComment
                    : model.Comments + " (" + DateTime.Now.ToString() + " " + User.Identity.Name.Split('\\')[1] + ") " + model.AddComment
                };
                db.Subtask_Recurrence.Attach(modelToEdit);
                var entry = db.Entry(modelToEdit);
                entry.Property(e => e.Comments).IsModified = true;
                db.SaveChanges();
                return RedirectToAction("EditSubtaskComments", model.ID);
            }
            return View(EditTaskComments(model.ID));
        }

        // GET: JobTask_Recurrence/ResetJobTask_Recurrence/5
        public ActionResult ResetJobTask_Recurrence(int? id, int? view)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewBag.ViewId = view;

            var model = (from jr in db.JobTask_Recurrence.Where(x => x.ID == id)
                         from t in db.JobTasks.Where(x => x.ID == jr.TaskID).DefaultIfEmpty()
                         select new JobTask_RecurrenceViewModel
                         {
                             ID = jr.ID,
                             Task = t.Title,
                             DueDate = jr.DueDate
                         }).FirstOrDefault();

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }
        public ActionResult FinishJobTask_Recurrence(int? id, int? view)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewBag.ViewId = view;

            var model = (from jr in db.JobTask_Recurrence.Where(x => x.ID == id)
                         from t in db.JobTasks.Where(x => x.ID == jr.TaskID).DefaultIfEmpty()
                         select new JobTask_RecurrenceViewModel
                         {
                             ID = jr.ID,
                             Task = t.Title,
                             DueDate = jr.DueDate
                         }).FirstOrDefault();

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }
        // POST: JobTask_Recurrence/ResetJobTask_RecurrenceConfirmed/5
        [HttpPost, ActionName("ResetJobTask_Recurrence")]
        [ValidateAntiForgeryToken]
        public ActionResult ResetJobTask_RecurrenceConfirmed([Bind(Include = "ID,AddComment")] JobTask_RecurrenceViewModel model, int? view)
        {
            if (model.AddComment != null && !string.IsNullOrEmpty(model.AddComment))
            {
                JobTask_Recurrence jobTask_Recurrence = db.JobTask_Recurrence.Find(model.ID);
                jobTask_Recurrence.Completed = false;
                jobTask_Recurrence.Finished = null;
                jobTask_Recurrence.FinishedBy = null;
                jobTask_Recurrence.Started = null;
                jobTask_Recurrence.StartedBy = null;
                jobTask_Recurrence.Comments = String.IsNullOrEmpty(jobTask_Recurrence.Comments)
                    ? "(" + DateTime.Now.ToString() + " " + User.Identity.Name.Split('\\')[1] + ") " + model.AddComment
                    : jobTask_Recurrence.Comments + " (" + DateTime.Now.ToString() + " " + User.Identity.Name.Split('\\')[1] + ") " + model.AddComment;
                var subTask_Recurrence = db.Subtask_Recurrence.Where(x => x.ParentID == jobTask_Recurrence.TaskID && x.DueDate == jobTask_Recurrence.DueDate).ToList();
                foreach (var item in subTask_Recurrence)
                {
                    item.Completed = false;
                    item.Finished = null;
                    item.FinishedBy = null;
                    item.Started = null;
                    item.StartedBy = null;
                }
                db.SaveChanges();
                return RedirectToAction("Index", new { id = view });
            }
            else
            {
                TempData["Comment"] = "Comments can't be empty when attempting to reset a task.";                
                return RedirectToAction("ResetJobTask_Recurrence", new { id = model.ID, view = view });
            }
        }
        [HttpPost, ActionName("FinishJobTask_Recurrence")]
        public ActionResult FinishJobTask_RecurrenceConfirmed([Bind(Include = "ID,AddComment")] JobTask_RecurrenceViewModel model, int? view)
        {
            if (model.AddComment != null && !string.IsNullOrEmpty(model.AddComment))
            {
                // Add Comments
                JobTask_Recurrence jobTask_Recurrence = db.JobTask_Recurrence.Find(model.ID);
                jobTask_Recurrence.Comments = String.IsNullOrEmpty(jobTask_Recurrence.Comments)
                    ? "(" + DateTime.Now.ToString() + " " + User.Identity.Name.Split('\\')[1] + ") " + model.AddComment
                    : jobTask_Recurrence.Comments + " (" + DateTime.Now.ToString() + " " + User.Identity.Name.Split('\\')[1] + ") " + model.AddComment;
                
                db.SaveChanges();
 
                return RedirectToAction("TaskFinished", new { id = model.ID });
            }
            else
            {
                TempData["Comment"] = "Comments can't be empty when attempting to Finish a task with open subtask(s).";
                return RedirectToAction("FinishJobTask_Recurrence", new { id = model.ID, view });
            }
        }
        // GET: JobTask_Recurrence/ResetSubtask_Recurrence/5
        public ActionResult ResetSubtask_Recurrence(int? pid, int? sid, int? view)
        {
            if (sid == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }          

            ViewBag.ViewId = view;

            var model = (from jr in db.Subtask_Recurrence.Where(x => x.ID == sid)
                         from t in db.Subtasks.Where(x => x.ID == jr.SubtaskID).DefaultIfEmpty()
                         select new Subtask_RecurrenceViewModel
                         {
                             ID = jr.ID,
                             ParentID = pid,
                             Subtask = t.Title,
                             DueDate = jr.DueDate
                         }).FirstOrDefault();

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: JobTask_Recurrence/ResetSubtask_RecurrenceConfirmed/5
        [HttpPost, ActionName("ResetSubtask_Recurrence")]
        [ValidateAntiForgeryToken]
        public ActionResult ResetSubtask_RecurrenceConfirmed([Bind(Include = "ID,ParentID,AddComment")] Subtask_RecurrenceViewModel model, int? view)
        {
            if (model.AddComment != null && !string.IsNullOrEmpty(model.AddComment))
            {
                JobTask_Recurrence jobTask_Recurrence = db.JobTask_Recurrence.Find(model.ParentID);
                jobTask_Recurrence.Completed = false;
                jobTask_Recurrence.Finished = null;
                jobTask_Recurrence.FinishedBy = null;
                Subtask_Recurrence subTask_Recurrence = db.Subtask_Recurrence.Find(model.ID);
                subTask_Recurrence.Completed = false;
                subTask_Recurrence.Finished = null;
                subTask_Recurrence.FinishedBy = null;
                subTask_Recurrence.Comments = String.IsNullOrEmpty(subTask_Recurrence.Comments)
                    ? "(" + DateTime.Now.ToString() + " " + User.Identity.Name.Split('\\')[1] + ") " + model.AddComment
                    : subTask_Recurrence.Comments + " (" + DateTime.Now.ToString() + " " + User.Identity.Name.Split('\\')[1] + ") " + model.AddComment;               
                db.SaveChanges();
                return RedirectToAction("Index", new { id = view });
            }
            else
            {
                TempData["Comment"] = "Comments can't be empty when attempting to reset a subtask.";                
                return RedirectToAction("ResetSubtask_Recurrence", new { pid = model.ParentID, sid = model.ID, view = view });
            }
        }

        // GET: JobTask_Recurrence/DeleteJobTask_Recurrence/5
        public ActionResult DeleteJobTask_Recurrence(int? id, int? view)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewBag.ViewId = view;
            
            var model = (from jr in db.JobTask_Recurrence.Where(x => x.ID == id)
                        from t in db.JobTasks.Where(x => x.ID == jr.TaskID).DefaultIfEmpty()
                        select new JobTask_RecurrenceViewModel
                        {
                            ID = jr.ID,
                            Task = t.Title,
                            DueDate = jr.DueDate
                        }).FirstOrDefault();

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: JobTask_Recurrence/DeleteJobTask_RecurrenceConfirmed/5
        [HttpPost, ActionName("DeleteJobTask_Recurrence")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteJobTask_RecurrenceConfirmed(int? id, int? view)
        {
            JobTask_Recurrence jobTask_Recurrence = db.JobTask_Recurrence.Find(id);
            db.JobTask_Recurrence.Remove(jobTask_Recurrence);
            var subTask_Recurrence = db.Subtask_Recurrence.Where(x => x.ParentID == jobTask_Recurrence.TaskID && x.DueDate == jobTask_Recurrence.DueDate).ToList();
            foreach (var item in subTask_Recurrence)
            {
                db.Subtask_Recurrence.Remove(item);
            }
            db.SaveChanges();
            return RedirectToAction("Index", new { id = view });
        }

        // GET: JobTask_Recurrence/DeleteSubtask_Recurrence/5
        public ActionResult DeleteSubtask_Recurrence(int? id, int? view)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewBag.ViewId = view;

            var model = (from sjr in db.Subtask_Recurrence.Where(x => x.ID == id)
                        from s in db.Subtasks.Where(x => x.ID == sjr.SubtaskID).DefaultIfEmpty()
                         select new Subtask_RecurrenceViewModel
                        {
                            ID = sjr.ID,
                            Subtask = s.Title,
                            DueDate = sjr.DueDate
                        }).FirstOrDefault();

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        // POST: JobTask_Recurrence/DeleteSubtask_RecurrenceConfirmed/5
        [HttpPost, ActionName("DeleteSubtask_Recurrence")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteSubtask_RecurrenceConfirmed(int? id, int? view)
        {
            Subtask_Recurrence subtask_Recurrence = db.Subtask_Recurrence.Find(id);
            db.Subtask_Recurrence.Remove(subtask_Recurrence);
            db.SaveChanges();
            return RedirectToAction("Index", new { id = view });
        }

        public ActionResult TaskStarted(int id)
        {  
            var modelToEdit = new JobTask_Recurrence();
            modelToEdit.ID = id;
            modelToEdit.Started = DateTime.Now;
            modelToEdit.StartedBy = User.Identity.Name.Split('\\')[1];
            db.JobTask_Recurrence.Attach(modelToEdit);
            var entry = db.Entry(modelToEdit);
            entry.Property(e => e.Started).IsModified = true;
            entry.Property(e => e.StartedBy).IsModified = true;
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult SubtaskStarted(int pid, int sid)
        {            
            var jobTask_Recurrence = (from jr in db.JobTask_Recurrence.Where(x => x.ID == pid)
                                     select jr).AsNoTracking().FirstOrDefault();

            if (jobTask_Recurrence.StartedBy == null)
            {
                var modelParentTaskToEdit = new JobTask_Recurrence();
                modelParentTaskToEdit.ID = pid;
                modelParentTaskToEdit.Started = DateTime.Now;
                modelParentTaskToEdit.StartedBy = User.Identity.Name.Split('\\')[1];
                db.JobTask_Recurrence.Attach(modelParentTaskToEdit);
                var taskEntry = db.Entry(modelParentTaskToEdit);
                taskEntry.Property(e => e.Started).IsModified = true;
                taskEntry.Property(e => e.StartedBy).IsModified = true;
            }            

            var modelSubtaskToEdit = new Subtask_Recurrence();
            modelSubtaskToEdit.ID = sid;
            modelSubtaskToEdit.Started = DateTime.Now;
            modelSubtaskToEdit.StartedBy = User.Identity.Name.Split('\\')[1];
            db.Subtask_Recurrence.Attach(modelSubtaskToEdit);
            var subTaskEntry = db.Entry(modelSubtaskToEdit);
            subTaskEntry.Property(e => e.Started).IsModified = true;
            subTaskEntry.Property(e => e.StartedBy).IsModified = true;

            db.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult TaskFinished(int id)
        {
            var taskValidation = (from jr in db.JobTask_Recurrence.Where(x => x.ID == id)
                                  from t in db.JobTasks.Where(x => x.ID == jr.TaskID).DefaultIfEmpty()
                                  from jd in db.JobDescriptions.Where(x => x.ID == t.JobDescriptionID).DefaultIfEmpty()
                                  select new JobTask_RecurrenceViewModel
                                  {
                                      Task = t.Title,
                                      DueDate = jr.DueDate,
                                      FactorNameData = jr.FactorNameData,
                                      FactorName = jd.FactorName
                                  }).AsNoTracking().FirstOrDefault();

            bool allowSave = false;

            if (String.IsNullOrEmpty(taskValidation.FactorName) || taskValidation.FactorName == "N/A"
                || taskValidation.FactorName == "n/a")
            {
                allowSave = true;
            }
            else
            {
                if (!String.IsNullOrEmpty(taskValidation.FactorNameData))
                {
                    allowSave = true;
                }
            }

            if (!allowSave)
            {
                TempData["FactorNameData"] = "FactorNameData can't be empty when attempting to finish task " + taskValidation.Task + " (" + taskValidation.DueDate + ").";
            }
            else
            {
                var modelToEdit = new JobTask_Recurrence();
                modelToEdit.ID = id;
                modelToEdit.Finished = DateTime.Now;
                modelToEdit.FinishedBy = User.Identity.Name.Split('\\')[1];
                modelToEdit.Completed = true;
                db.JobTask_Recurrence.Attach(modelToEdit);
                var entry = db.Entry(modelToEdit);
                entry.Property(e => e.Finished).IsModified = true;
                entry.Property(e => e.FinishedBy).IsModified = true;
                entry.Property(e => e.Completed).IsModified = true;
                db.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        [HttpPost, ActionName("requireCommentsToFinish")]
        public ActionResult requireCommentsToFinish(int id)
        {
            //bool isIncomplete = false;
            var subtasks = (from jt in db.JobTask_Recurrence.Where(j=>j.ID==id)
                            from jsr in db.Subtask_Recurrence.Where(x => x.ParentID == jt.TaskID)
                                     from s in db.Subtasks.Where(x => x.ID == jsr.SubtaskID).DefaultIfEmpty()
                                     from jd in db.JobDescriptions.Where(x => x.ID == s.JobDescriptionID).DefaultIfEmpty()
                                     select new Subtask_RecurrenceViewModel
                                     {
                                         Subtask = s.Title,
                                         DueDate = jsr.DueDate,
                                         FactorNameData = jsr.FactorNameData,
                                         FactorName = jd.FactorName
                                     }).AsNoTracking().FirstOrDefault();

            /** If there are subtasks we need to get comments**/
            if (subtasks!=null)
            {
                return RedirectToAction("FinishJobTask_Recurrence", new
                {
                    id = id,
                    view = 1
                });
            }
            /** No subtasks so we will just finish it **/
            return RedirectToAction("TaskFinished",new
            {
                id = id
            });
            //return Json(new { incomplete = isIncomplete });
        }
        public ActionResult SubtaskFinished(int pid, int sid)
        {
            //var taskValidation = (from jr in db.JobTask_Recurrence.Where(x => x.ID == pid)
            //                      from t in db.JobTasks.Where(x => x.ID == jr.TaskID).DefaultIfEmpty()
            //                      from jd in db.JobDescriptions.Where(x => x.ID == t.JobDescriptionID).DefaultIfEmpty()
            //                      select new JobTask_RecurrenceViewModel
            //                      {
            //                          Task = t.Title,
            //                          DueDate = jr.DueDate,
            //                          FactorNameData = jr.FactorNameData,
            //                          FactorName = jd.FactorName
            //                      }).AsNoTracking().FirstOrDefault();

            var subtaskValidation = (from jsr in db.Subtask_Recurrence.Where(x => x.ID == sid)
                                  from s in db.Subtasks.Where(x => x.ID == jsr.SubtaskID).DefaultIfEmpty()
                                  from jd in db.JobDescriptions.Where(x => x.ID == s.JobDescriptionID).DefaultIfEmpty()
                                  select new Subtask_RecurrenceViewModel
                                  {
                                      Subtask = s.Title,                                       
                                      DueDate = jsr.DueDate,
                                      FactorNameData = jsr.FactorNameData,
                                      FactorName = jd.FactorName
                                  }).AsNoTracking().FirstOrDefault();

            //bool taskAllowSave = false;
            bool subTaskAllowSave = false;

            //if (String.IsNullOrEmpty(taskValidation.FactorName) || taskValidation.FactorName == "N/A"
            //  || subtaskValidation.FactorName == "n/a")
            //{
            //    taskAllowSave = true;
            //}
            //else
            //{
            //    if (!String.IsNullOrEmpty(taskValidation.FactorNameData))
            //    {
            //        taskAllowSave = true;
            //    }
            //}

            if (String.IsNullOrEmpty(subtaskValidation.FactorName) || subtaskValidation.FactorName == "N/A"
               || subtaskValidation.FactorName == "n/a")
            {
                subTaskAllowSave = true;
            }
            else
            {
                if (!String.IsNullOrEmpty(subtaskValidation.FactorNameData))
                {
                    subTaskAllowSave = true;
                }
            }

            if (!subTaskAllowSave)           
            {
                TempData["FactorNameData"] = "FactorNameData can't be empty when attempting to finish subtask " + subtaskValidation.Subtask + " (" + subtaskValidation.DueDate + ").";                
            }
            //else if (!taskAllowSave)
            //{
            //    TempData["FactorNameData"] = "FactorNameData can't be empty when attempting to finish task " + taskValidation.Task + " (" + taskValidation.DueDate + ").";
            //}
            else
            {
                var activeSubtasks = (from jr in db.JobTask_Recurrence.Where(x => x.ID == pid)
                                      from jsr in db.Subtask_Recurrence.Where(x => (x.ParentID == jr.TaskID && x.DueDate == jr.DueDate)).DefaultIfEmpty()
                                      where
                                      jr.ID == pid
                                      && jsr.ParentID == jr.TaskID
                                      && jsr.DueDate == jr.DueDate
                                      && jsr.Completed == false
                                      select jsr).AsNoTracking().ToList();

                if (activeSubtasks.Count == 1)
                {
                    var modelParentTaskToEdit = new JobTask_Recurrence();
                    modelParentTaskToEdit.ID = pid;
                    modelParentTaskToEdit.Finished = DateTime.Now;
                    modelParentTaskToEdit.FinishedBy = User.Identity.Name.Split('\\')[1];
                    modelParentTaskToEdit.Completed = true;
                    db.JobTask_Recurrence.Attach(modelParentTaskToEdit);
                    var taskEntry = db.Entry(modelParentTaskToEdit);
                    taskEntry.Property(e => e.Finished).IsModified = true;
                    taskEntry.Property(e => e.FinishedBy).IsModified = true;
                    taskEntry.Property(e => e.Completed).IsModified = true;
                }

                var modelSubtaskToEdit = new Subtask_Recurrence();
                modelSubtaskToEdit.ID = sid;
                modelSubtaskToEdit.Finished = DateTime.Now;
                modelSubtaskToEdit.FinishedBy = User.Identity.Name.Split('\\')[1];
                modelSubtaskToEdit.Completed = true;
                db.Subtask_Recurrence.Attach(modelSubtaskToEdit);
                var subTaskentry = db.Entry(modelSubtaskToEdit);
                subTaskentry.Property(e => e.Finished).IsModified = true;
                subTaskentry.Property(e => e.FinishedBy).IsModified = true;
                subTaskentry.Property(e => e.Completed).IsModified = true;
                db.SaveChanges();               
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }        
    }
}
