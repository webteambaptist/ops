﻿using OPSTasks.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OPSTasks.Models.Subtask_LookupViewModels
{
    public class Subtask_LookupViewModel
    {
        public int ID { get; set; }

        public Nullable<int> JobTaskID { get; set; }

        public Nullable<int> SubtaskID { get; set; }

        public Nullable<int> OrderNumber { get; set; }

        public Nullable<System.DateTime> Created { get; set; }

        public string CreatedBy { get; set; }

        public string JobTask { get; set; }

        public string Subtask { get; set; }

        public List<SelectListItem> Subtasks { get; set; }
    }
}