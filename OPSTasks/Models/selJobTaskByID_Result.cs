//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OPSTasks.Models
{
    using System;
    
    public partial class selJobTaskByID_Result
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public Nullable<int> JobDescriptionID { get; set; }
        public string Frequency { get; set; }
        public Nullable<int> Intervals { get; set; }
        public Nullable<System.DateTime> Created { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<int> ReplaceTaskID { get; set; }
        public Nullable<System.DateTime> Modified { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<bool> HasSubtasks { get; set; }
        public string Month { get; set; }
        public Nullable<int> Day { get; set; }
        public string Placement { get; set; }
    }
}
