﻿using OPSTasks.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OPSTasks.Models.SubTask_RecurrenceViewModels
{
    public class Subtask_RecurrenceViewModel
    {
        public Nullable<int> ID { get; set; }

        public Nullable<int> SubtaskID { get; set; }

        public Nullable<System.DateTime> Started { get; set; }

        public string StartedBy { get; set; }

        public Nullable<System.DateTime> Finished { get; set; }

        public string FinishedBy { get; set; }

        public Nullable<int> ParentID { get; set; }

        public Nullable<System.DateTime> DueDate { get; set; }

        public string FactorNameData { get; set; }

        public Nullable<bool> Completed { get; set; }

        public string Comments { get; set; }

        public Nullable<bool> IsActive { get; set; }

        public Nullable<System.DateTime> Created { get; set; }

        public string CreatedBy { get; set; }

        public Nullable<System.DateTime> Modified { get; set; }

        public string ModifiedBy { get; set; }

        public string Subtask { get; set; }

        public string FactorName { get; set; }

        public string AddComment { get; set; }

        public string AddFactorNameData { get; set; }

        public Nullable<int> JobDescriptionID { get; set; }

        public string JobDescriptionURL { get; set; }

        public int? OrderNumber { get; set; }

        public string Title1 { get; set; }
        public string Title2 { get; set; }
        public string Title3 { get; set; }
        public string Title4 { get; set; }
    }
}