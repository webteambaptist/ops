﻿using OPSTasks.Helpers;
using OPSTasks.Models.SubTask_RecurrenceViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OPSTasks.Models.JobTask_RecurrenceViewModels
{
    public class JobTask_RecurrenceViewModel
    {
        public int ID { get; set; }

        public Nullable<int> TaskID { get; set; }

        public Nullable<System.DateTime> DueDate { get; set; }

        public string FactorNameData { get; set; }

        public Nullable<System.DateTime> Started { get; set; }

        public string StartedBy { get; set; }

        public Nullable<System.DateTime> Finished { get; set; }

        public string FinishedBy { get; set; }

        public Nullable<bool> Completed { get; set; }

        public string Comments { get; set; }

        public Nullable<bool> IsActive { get; set; }

        public Nullable<System.DateTime> Created { get; set; }

        public string CreatedBy { get; set; }

        public Nullable<System.DateTime> Modified { get; set; }

        public string ModifiedBy { get; set; }

        public string Task { get; set; }

        public IEnumerable<Subtask_RecurrenceViewModel> Subtasks { get; set; }

        public List<SelectListItem> Views { get; set; }

        public string View { get; set; }
        
        public string FactorName { get; set; }

        public string AddComment { get; set; }

        public string AddFactorNameData { get; set; }

        public Nullable<int> JobDescriptionID { get; set; }

        public string JobDescriptionURL { get; set; }
        [DisplayName("")]
        public string Title1 { get; set; }
        [DisplayName("")]
        public string Title2 { get; set; }
        [DisplayName("")]
        public string Title3 { get; set; }
        [DisplayName("Task")]
        public string Title4 { get; set; }
    }
}