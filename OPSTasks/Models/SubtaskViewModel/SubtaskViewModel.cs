﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OPSTasks.Models
{
    public class SubtaskViewModel
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public Nullable<int> JobDescriptionID { get; set; }
        public Nullable<System.DateTime> Created { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> Modified { get; set; }
        public string ModifiedBy { get; set; }
        public string JobDescription { get; set; }
        public string JobDescriptionURL { get; set; }
        public List<SelectListItem> JobDescriptions { get; set; }
    }
}