﻿using OPSTasks.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OPSTasks.Models.JobTaskViewModels
{
    public class JobTaskViewModel
    {
        public int ID { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public Nullable<System.DateTime> Created { get; set; }

        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> Modified { get; set; }

        public string ModifiedBy { get; set; }

        public Nullable<int> JobDescriptionID { get; set; }

        public string Frequency { get; set; }

        public Nullable<int> Intervals { get; set; }

        public Nullable<System.DateTime> StartDate { get; set; }

        public Nullable<System.DateTime> EndDate { get; set; }

        public Nullable<int> ReplaceTaskID { get; set; }

        public string JobDescriptionURL { get; set; }

        public bool HasSubtasks { get; set; }

        public string ReplaceTask { get; set; }

        public Nullable<int> Day { get; set; }

        public string Month { get; set; }

        public string Placement { get; set; }       

        public List<CheckBoxList> DaysOfWeek { get; set; }

        public List<SelectListItem> JobDescriptions { get; set; }

        public string JobDescription { get; set; }

        public List<SelectListItem> Frequencies { get; set; }        

        public List<SelectListItem> Months { get; set; }

        public List<SelectListItem> Placements { get; set; }

        public List<SelectListItem> ReplacementTasks { get; set; }
        public Boolean byDate { get; set; }
    }
}