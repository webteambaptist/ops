﻿using OPSTasks.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OPSTasks.Models.JobDescriptionViewModels
{
    public class JobDescriptionViewModel
    {
        public int ID { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Action { get; set; }

        public string Category { get; set; }

        public Nullable<bool> AllowSkip { get; set; }

        public string FactorName { get; set; }

        public Nullable<System.DateTime> Created { get; set; }

        public string CreatedBy { get; set; }

        public Nullable<System.DateTime> Modified { get; set; }

        public string ModifiedBy { get; set; }
        
        public List<SelectListItem> Actions { get; set; }

        public List<SelectListItem> Categories { get; set; }

        public List<SelectListItem> FactorNames { get; set; }
    }
}