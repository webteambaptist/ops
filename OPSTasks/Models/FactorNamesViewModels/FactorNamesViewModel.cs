﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OPSTasks.Models.FactorNamesViewModels
{
    public class FactorNamesViewModel
    {
        public int ID { get; set; }
        public string FactorName { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public string CreatedBy {get;set;}
        public string UpdatedBy { get; set; }
        public int Deleted { get; set; }
    }
}