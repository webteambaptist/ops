﻿//DEV: When publishing dont forget to to add the Operations/ back in front of the url. But before publishing, make sure to add the /Operations back to the URL for the MVC Controller access.
//QA: When publishing dont forget to to add the ops / back in front of the url.But before publishing, make sure to add the /ops back to the URL for the MVC Controller access.

$(document).scroll(function () {
    localStorage['page'] = document.URL;
    localStorage['scrollTop'] = $(document).scrollTop();
});
$(document).ready(function () {
    if (localStorage['page'] == document.URL) {
        $(document).scrollTop(localStorage['scrollTop']);
    }

});
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}
$(function () {
    var date = new Date();
    date.setDate(date.getDate() - 30);
    $('#start').datetimepicker({
        viewMode: 'days',
        format: 'YYYY-MM-DD',
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        }
    });
    $('#end').datetimepicker({
        useCurrent: false,
        viewMode: 'days',
        format: 'YYYY-MM-DD',

        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        }
    });
    $("#start").on("dp.change", function (e) {
        if (e.oldDate != null) {
            // clear end dates
            $("#endDate").val('');
            var minDate = new Date();
            minDate = formatDate(e.date);

            var endDate = new Date(minDate);
            endDate.setDate(endDate.getDate() + 30);

            $('#end').data("DateTimePicker").minDate(minDate);
            $('#end').data("DateTimePicker").maxDate(endDate);
        }
    });
});
function search() {
    $('#searchButton').prop('disabled', true);
    var startDate = $("#startDate").val();
    var endDate = $("#endDate").val();
    $.ajax({
        
        type: "POST",
        url: '/JobTask_Recurrence/Index',
        //url: '/OPS/JobTask_Recurrence/Index',
        data: { id: "4", startDate: startDate, endDate: endDate },
        success: function (model) {
            $("body").html(model);
            $('#searchButton').prop('disabled', false);
            $("#start").datetimepicker({
                date: startDate,
                viewMode: 'days',
                format: 'YYYY-MM-DD'
            });
            $("#end").datetimepicker({
                date: endDate,
                viewMode: 'days',
                format: 'YYYY-MM-DD'
            });
        }
    });
}
function requireComments(id) {
    // are there sub tasks
    $.ajax({
        type: "POST",
        url: '/JobTask_Recurrence/requireCommentsToFinish',
        //url: '/OPS/JobTask_Recurrence/requireCommentsToFinish',
        data: {id:id},
        success: function (model) {
            $("body").html(model);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
          // alert("Error: " + errorThrown);
        }    
    })
}

function finishTask(id) {
    $.ajax({
        type: "POST",
        url: '/JobTask_Recurrence/TaskFinished',
        //url: '/OPS/JobTask_Recurrence/TaskFinished',
        data: { id: id },
        success: function (model) {
            $("body").html(model);
            //alert("success");
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
          //  alert("Error: " + errorThrown);
        }  
    });
}
function submitComments(id, view) {
   // alert($("#comments").val());
    var addComments = $("#AddCommentsID").val();
    var comments = $("#comments").val();
    $.ajax({
        type: "POST",
        url: '/JobTask_Recurrence/FinishJobTask_RecurrenceConfirmed',
        //url: '/OPS/JobTask_Recurrence/FinishJobTask_RecurrenceConfirmed',
        data: { ID: id, comments: comments, AddComments: addComments, view: view },
        success: function (model) {
            $("body").html(model);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
        //    alert("Error: " + errorThrown);
        }
    });
}
function getComments(id, view) {
    $.ajax({
        type: "POST",
        url: '/JobTask_Recurrence/FinishJobTask_Recurrence',
        //url: '/OPS/JobTask_Recurrence/FinishJobTask_Recurrence',
        data: { ID: id, view: view },
        success: function (model) {
            $("body").html(model);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
          //  alert("Error: " + errorThrown);
        }
    });
}

function refresh(apiAddress) {
    var url = apiAddress + 'api/task/update';
    if (window.XDomainRequest) {
        if (window.XDomainRequest) {
            var xdr = new XDomainRequest();
            if (xdr) {
                xdr.open('GET', url);
                xdr.send();
               // location.href = location.href;
            }
        }
    }
    else {
        $.ajax({
            type: "GET",
            url: url,
            contentType: 'text/plain',
            xhrFields: {
                withCredentials: false
            },
            success: function (data) {
                alert(data);
            },
            error: function (data) {
                alert(data.responseText);
            }
        });
        location.reload();
        //location.href = location.href;
    }
}

function DisplayProgressMessage(ctl, msg, apiAddress) {
    $(ctl).prop("disabled", true).text(msg);
    $(".submit-progress").removeClass("hidden");
    $("body").addClass("submit-progress-bg");
    //// call refresh
    refresh(apiAddress);
    $("body").removeClass("submit-progress-bg");
    $(ctl).prop("disabled", false).text(msg);
    $(".submit-progress").addClass("hidden");
    return true;
}
/**
 * This is commented out until we push Sprint 2. */
function deleteWeek(apiAddress) {
    var url = apiAddress + 'api/task/delete';
    var url2 = '/JobTask_Recurrence/Index/' + '?id=' + 2;
    //if (window.XDomainRequest) {
    //    if (window.XDomainRequest) {
    //        var xdr = new XDomainRequest();
    //        if (xdr) {
    //            xdr.open('POST', url);
    //            xdr.send();
    //            location.href = url2;
    //            //location.reload(true);
    //            //location.href = location.href + '/4';
    //        }
    //    }
    //}
    if (window.XDomainRequest) {
        // var xdr = new XDomainRequest();
        var xhr = new XMLHttpRequest();
        if (xhr) {
            xhr.open('POST', url, true);
            xhr.send();
            // location.href = url2;
            //location.href = location.href;
        }
    }
    else {
        $.ajax({
            type: "POST",
            url: url
        });
        //location.reload();
        //return true;
        //location.href = location.href;
    //location.reload();
    //location.href = location.href;
    }
}
function DisplayProgressMessageForDelete(ctl, msg, apiAddress) {
    $(ctl).prop("disabled", true).text(msg);
    $(".submit-progress2").removeClass("hidden");
    $("body").addClass("submit-progress-bg");
    // call deleteWeek
    deleteWeek(apiAddress);
    if (window.XDomainRequest) {
        location.href = 'http://qabhwebapps.bh.local/ops/JobTask_Recurrence/Index/?id=2';
        //location.reload();
    }
    else {
        location.reload();
    }
    // $("body").removeClass("submit-progress-bg");
    // $(ctl).prop("disabled", false).text(msg);
    // $(".submit-progress2").addClass("hidden");
    return true;
}
