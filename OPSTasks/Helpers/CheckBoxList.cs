﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OPSTasks.Helpers
{
    public class CheckBoxList
    {
        public int id { get; set; }

        public string text { get; set; }

        public bool isChecked { get; set; }
    }
}